# Bookinst Customer App

##To run your app in the browser (great for initial development):
`ionic serve`

##To run on iOS:
`ionic run ios`

##To run on Android:
`ionic run android`

![Screenshot](./src/assets/screenshot.png)
