import { IonicErrorHandler, ToastController } from 'ionic-angular';
import { NgModule, ErrorHandler, Inject } from '@angular/core';
import postal from 'postal';
export class MyErrorHandler implements ErrorHandler {
  networkConnected : boolean

  constructor(@Inject(ToastController) public toastCtrl: ToastController) {
    this.networkConnected = false;
    this.toastCtrl = toastCtrl;
    postal.subscribe({
      channel: 'App',
      topic: 'Network_Error',
      callback: (data) => {
        this.networkConnected = true
      }
    })
    postal.subscribe({
      channel: 'App',
      topic: 'Network_Resume',
      callback: (data) => {
        this.networkConnected = false
      }
    })
  }

  handleError(err: any): void {
    // do something with the error
    setTimeout(() => {
      postal.publish({
        channel: 'App',
        topic: 'Stop_Loading',
      })
    }, 500);
    console.log(err);
    let errorMsg = null;
    if(err.rejection && err.rejection.message){
      errorMsg = err.rejection.message
    }else
      errorMsg = err;

    if(errorMsg instanceof Object){

    }else{
      if(!this.networkConnected){
        let toast = this.toastCtrl.create({
          message: errorMsg,
          duration: 3000
        });
        toast.present();
      }
    }
  }
}
