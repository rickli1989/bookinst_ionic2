import { Injectable } from "@angular/core";

@Injectable()
export class UtilService {
  constructor() {}

  getDefaultStoreImg() {
    return './assets/images/default/placeholder.png';
  }

  getDefaultUserImg() {
    return './assets/images/default/avatar.jpeg';
  }
}
