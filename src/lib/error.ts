import { NgModule } from '@angular/core';
import postal from 'postal';
import { ToastController } from 'ionic-angular';

@NgModule({
})
export class ErrorModule { 
  constructor(public toastCtrl: ToastController) {
    postal.subscribe({
      channel: 'App',
      topic: 'Error',
      callback: (data) => {
        let toast = this.toastCtrl.create({
          message: data,
          duration: 3000
        });
        toast.present();
      }
    })
  }
}