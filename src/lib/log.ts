// Import the core angular services.
import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { Response } from "@angular/http";

@Injectable()
export class ErrorLogService {

    private http: Http;


    // I initialize the service.
    constructor( http: Http ) {

        this.http = http;

    }


    // I log the given error to various aggregation and tracking services.
    public logError( error: any ) : void {

        // Internal tracking.
        this.sendToConsole( error );

    }


    // I send the error the browser console (safely, if it exists).
    private sendToConsole(error: any): void {

        if ( console && console.group && console.error ) {

            console.group( "Error Log Service" );
            console.error( error );
            console.error( error.message );
            console.error( error.stack );
            console.groupEnd();

        }

    }



}