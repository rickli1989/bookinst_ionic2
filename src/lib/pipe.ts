import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'matchedName' })
export class matchedNamePipe implements PipeTransform {
    transform(allNames: any[],pattern:string) {
        if(allNames.length){
            console.log('refreshing',allNames,'p',pattern)

            return allNames.filter(name => name.name.toLowerCase().includes(pattern?pattern.toLowerCase():''));

        }else {

            return allNames;
        }
    }
}

@Pipe({ name: 'matchedStaffName' })
export class matchedStaffNamePipe implements PipeTransform {
    transform(allNames: any[],pattern:string) {
        if(allNames.length){
            console.log('refreshing',allNames,'p',pattern)

            return allNames.filter(name => name.lastname.toLowerCase().includes(pattern?pattern.toLowerCase():'')||name.firstname.toLowerCase().includes(pattern?pattern.toLowerCase():''));

        }else {

            return allNames;
        }
    }
}