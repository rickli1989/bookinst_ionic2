import { Headers, RequestOptions } from '@angular/http';
import { API_URL } from '../config/app.config';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';



export const util = {

  getCurrentLocation: async(storage, geolocation) => {

    // await storage.remove("Self_Location");
    let selfLocation = await storage.get("Self_Location");

    if (selfLocation) {
      return selfLocation;
    } else {
      let location = await geolocation.getCurrentPosition();
      selfLocation = {lat: location.coords.latitude, lng: location.coords.longitude};
      await storage.set("Self_Location", selfLocation);

      return selfLocation;
    }

  },

  maybeProp: (key, obj, defaultVal = null) => (obj == null || typeof obj === 'undefined' || typeof obj[key] === 'undefined') ? ( defaultVal == null ? '' : defaultVal) : obj[key],

  getHttpHeader: async (http, storage) => {
    let token = await util.getAccessToken(http, storage);

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });

    let options = new RequestOptions({ headers: headers });

    return options;
  },

  handleError: (error: any): Promise<any> => {
    return Promise.resolve(error.message || error);
  },

  generatePassword: () => {
    var length = 8,
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  },

  getUrl: () => {
    return (src) => {
      return src ? ( src.startsWith("http") ? src : `https:${src}` ) : "";
    }
  },

  getAccessToken: async (http, storage) => {
    // let headers = await util.getHttpHeader(storage);
    let token = await storage.get("Access_Token");
    let accessheaders = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });

    let accessoptions = new RequestOptions({ headers: accessheaders });

    let responseAccessToken = await http.post(`${API_URL}/client/auth/check`, {}, accessoptions)
        .timeout(3000)
        .toPromise()
      .catch(util.handleError);

    if(responseAccessToken.status != 200){
      let refreshtoken = await storage.get("Refresh_Token");

      let headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + refreshtoken
      });

      let options = new RequestOptions({ headers: headers });

      let response = await http.post(`${API_URL}/client/auth/refresh`, {
        token: refreshtoken
      }, options).toPromise().catch(util.handleError);

      if(response.status != 200)
        return null;
      else{
        let json = await response.json();
        await storage.set("Access_Token", json.accessToken.token);
        return json.accessToken.token;
      }
    }else{
      let token = await storage.get("Access_Token");
      return token;
    }

  }

}

    