// export const API_URL = 'https://test.bookinst.com.au/api';
export const API_URL = 'http://localhost:3005/api';
// export const API_URL = 'https://app.bookinst.com.au/api';

export const GOOGLE_MAP_API_KEY = 'AIzaSyDLip4oq3WS2JR_RAhqqNy1RfY0_1fu0u8';
export const FIREBASE_CLIENT_PASSOWRD = 'bookinst';

export namespace App {
  // Get your Firebase app's config on your Firebase console. "Add Firebase to your web app".
  export const firebaseConfig = {
    apiKey: "AIzaSyB2xeie7qMwkWk5ws8Xu4jnRED2MWxWvsQ",
    authDomain: "bookinst-951d5.firebaseapp.com",
    databaseURL: "https://bookinst-951d5.firebaseio.com",
    projectId: "bookinst-951d5",
    storageBucket: "bookinst-951d5.appspot.com",
    messagingSenderId: "311709244865"
  }

  export const firebaseConfigTest = {
    apiKey: "AIzaSyAgZnpIVefGj03OlgBDe_M_SiMne2_IwTo",
    authDomain: "bookinst-test.firebaseapp.com",
    databaseURL: "https://bookinst-test.firebaseio.com",
    projectId: "bookinst-test",
    storageBucket: "bookinst-test.appspot.com",
    messagingSenderId: "430754088286"
  }
}
