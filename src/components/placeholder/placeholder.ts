/**
 * Created by xiaoY on 8/3/17.
 */
import { Component, Input, ElementRef, Renderer, OnChanges, SimpleChange } from '@angular/core';

@Component({
    selector: 'placeholder',
    templateUrl: 'placeholder.html'
})
export class Placeholder implements OnChanges {
    t: string = '';
    @Input() title: string;


    constructor(public _elementRef: ElementRef, public _renderer: Renderer) {

    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        this._update();
        // console.log("CHANGES background-image", this._src);
        // console.log(changes['src'].isFirstChange());
    }

    _update() {


    }

    _loaded(isLoaded: boolean) {
    }
}