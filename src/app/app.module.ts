import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { ErrorModule } from '../lib/error';
import { ListingPage } from '../pages/listing/listing';
import { FeedPage } from '../pages/feed/feed';
import { LoginPage } from '../pages/login/login';
import { NotificationsPage } from '../pages/notifications/notifications';
import { ProfilePage } from '../pages/profile/profile';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { SettingsPage } from '../pages/settings/settings';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { SchedulePage } from '../pages/schedule/schedule';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { FavoritePage } from '../pages/favorite/favorite';
import { StoreProfilePage } from '../pages/store-profile/store-profile';
import { AppointmentPage } from '../pages/appointment/appointment'
import { BookingDetailPage } from '../pages/booking-detail/booking-detail';
import { MapFilterPage } from '../pages/map-filter/map-filter';

import { Placeholder } from '../components/placeholder/placeholder';
import { PreloadImage } from '../components/preload-image/preload-image';
import { BackgroundImage } from '../components/background-image/background-image';
import { ShowHideContainer } from '../components/show-hide-password/show-hide-container';
import { ShowHideInput } from '../components/show-hide-password/show-hide-input';
import { ColorRadio } from '../components/color-radio/color-radio';
import { CounterInput } from '../components/counter-input/counter-input';
import { Rating } from '../components/rating/rating';
import { GoogleMap } from '../components/google-map/google-map';
import { ImageModalPage } from '../components/image-modal/image-modal';

import { FeedService } from '../pages/feed/feed.service';
import { ListingService } from '../pages/listing/listing.service';
import { ProfileService } from '../pages/profile/profile.service';
import { NotificationsService } from '../pages/notifications/notifications.service';
import { ScheduleService } from '../pages/schedule/schedule.service';
import { LoginService } from '../pages/login/login.service';
import { SignUpService } from '../pages/signup/signup.service';
import { BookingPage } from '../pages/booking/booking';
import { FavoriteService } from '../pages/favorite/favorite.service';
import { MapsFilterService } from '../pages/map-filter/map-filter.service';
// Functionalities
import { MapsPage } from '../pages/maps/maps';
import { FacebookLoginPage } from '../pages/facebook-login/facebook-login';
import { GoogleLoginPage } from '../pages/google-login/google-login';
import { ContactCardPage } from '../pages/contact-card/contact-card';

import { FacebookLoginService } from '../pages/facebook-login/facebook-login.service';
import { GoogleLoginService } from '../pages/google-login/google-login.service';
import { GoogleMapsService } from '../pages/maps/maps.service';
//import { StoreMapService  } from '../pages/store-map/store-map.service';
import { StoreDetailService } from '../pages/contact-card/contact-card.service';
import { UtilService } from '../lib/service';
import { BookingService } from '../pages/booking/booking.service';
import { AppointmentService } from '../pages/appointment/appointment.service';
import { AppointmentModifyService } from '../pages/booking-detail/booking-detail.service';
import { IonicStorageModule } from '@ionic/storage';
import { NgCalendarModule  } from 'ionic2-calendar';
import { MultiPickerModule } from 'ion-multi-picker';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { matchedNamePipe, matchedStaffNamePipe } from '../lib/pipe';
import { ErrorLogService } from '../lib/log';
// import { MessagePage } from '../pages/messages/message/message';
// import { MessageService } from '../pages/messages/message/message.service';
// import { MessagesPage } from '../pages/messages/messages';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MyErrorHandler } from '../lib/error_handle';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Camera } from '@ionic-native/camera';
import { Keyboard } from '@ionic-native/keyboard';
import { Transfer } from '@ionic-native/transfer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { GooglePlus } from '@ionic-native/google-plus';
import { Network } from '@ionic-native/network';
import { NativeStorage } from '@ionic-native/native-storage';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
// import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { Facebook } from '@ionic-native/facebook';
import { FriendPipe } from '../pipes/friend';
import { SearchPipe } from '../pipes/search';
import { ConversationPipe } from '../pipes/conversation';
import { DateFormatPipe } from '../pipes/date';
import { GroupPipe } from '../pipes/group';
// import { UserInfoPage } from '../pages/messages/message/user-info/user-info';
// import { NewMessagePage } from '../pages/messages/new-message/new-message';
// import { SearchPeoplePage } from '../pages/messages/search-people/search-people';
import * as firebase from 'firebase';
import { AngularFireModule, AuthMethods, AuthProviders } from 'angularfire2';
import { LoadingProvider } from '../providers/loading';
import { AlertProvider } from '../providers/alert';
import { ImageProvider } from '../providers/image';
import { DataProvider } from '../providers/data';
import { FirebaseProvider } from '../providers/firebase';
import 'chart.js';
import { App } from '../config/app.config';
import 'promise-polyfill'; 

// To add to window
firebase.initializeApp(App.firebaseConfig);
// firebase.initializeApp(App.firebaseConfigTest);

@NgModule({
  declarations: [
    MyApp,
    ListingPage,
    FeedPage,
    LoginPage,
    NotificationsPage,
    ProfilePage,
    TabsNavigationPage,
    WalkthroughPage,
    SettingsPage,
    SignupPage,
    ForgotPasswordPage,
    SchedulePage,
    TermsOfServicePage,
    PrivacyPolicyPage,
    MapsPage,
    FacebookLoginPage,
    GoogleLoginPage,
    ContactCardPage,
    StoreProfilePage,
    Placeholder,
    PreloadImage,
    BackgroundImage,
    ShowHideContainer,
    ShowHideInput,
    ColorRadio,
    CounterInput,
    Rating,
    GoogleMap,
    FavoritePage,
    BookingPage,
    AppointmentPage,
    BookingDetailPage,
    matchedNamePipe,
    matchedStaffNamePipe,
    // MessagePage,
    ImageModalPage,
    MapFilterPage,
    FriendPipe,
    ConversationPipe,
    SearchPipe,
    DateFormatPipe,
    GroupPipe
    // MessagesPage,
    // UserInfoPage,
    // NewMessagePage,
    // SearchPeoplePage
  ],
  imports: [
    NgCalendarModule,
    MultiPickerModule,
    ErrorModule,
    ChartsModule,
    IonicStorageModule.forRoot(),
    IonicImageViewerModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
        scrollAssist: false, 
        autoFocusAssist: false
    }),
    AngularFireModule.initializeApp(App.firebaseConfig),
    // AngularFireModule.initializeApp(App.firebaseConfigTest),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListingPage,
    FeedPage,
    LoginPage,
    NotificationsPage,
    ProfilePage,
    TabsNavigationPage,
    WalkthroughPage,
    SettingsPage,
    ForgotPasswordPage,
    SignupPage,
    SchedulePage,
    TermsOfServicePage,
    PrivacyPolicyPage,
    MapsPage,
    FacebookLoginPage,
    GoogleLoginPage,
    ContactCardPage,
    StoreProfilePage,
    FavoritePage,
    BookingPage,
    AppointmentPage,
    BookingDetailPage,
    // MessagePage,
    ImageModalPage,
    MapFilterPage
    // MessagesPage,
    // UserInfoPage,
    // NewMessagePage,
    // SearchPeoplePage
  ],
  providers: [
    FeedService,
    ListingService,
    ProfileService,
    NotificationsService,
    ScheduleService,
    LoginService,
    SignUpService,
    FavoriteService,
    FacebookLoginService,
    GoogleLoginService,
    GoogleMapsService,
    StoreDetailService,
    UtilService,
    BookingService,
    AppointmentService,
    AppointmentModifyService,
    LaunchNavigator,
    ErrorLogService,
    // MessageService,
    MapsFilterService,
    SplashScreen,
    StatusBar,
    CallNumber,
    Network,
    SocialSharing,
    Geolocation,
    InAppBrowser,
    LaunchNavigator,
    PhotoViewer,
    Keyboard,
    GooglePlus,
    NativeStorage,
    Transfer,
    Camera,
    Facebook,
    Contacts,
    LoadingProvider, AlertProvider, ImageProvider, DataProvider, FirebaseProvider,
    NativePageTransitions,
    {
      provide: ErrorHandler, useClass: MyErrorHandler
    }  
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
