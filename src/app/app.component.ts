import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App, ToastController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Http } from '@angular/http';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { SettingsPage } from '../pages/settings/settings';
import { ListingPage } from '../pages/listing/listing';
import { Storage } from '@ionic/storage';
import { util } from '../lib/util';
import { Network } from '@ionic-native/network';
import { Geolocation } from '@ionic-native/geolocation';
import * as firebase from 'firebase';
import { Keyboard } from '@ionic-native/keyboard';
import { API_URL } from '../config/app.config';
import postal from 'postal';
@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  // make WalkthroughPage the root (or first) page
  rootPage: any = null;
  // rootPage: any = TabsNavigationPage;


  pages: Array<{title: string, icon: string, component: any}>;
  pushPages: Array<{title: string, icon: string, component: any}>;

  constructor(
    platform: Platform,
    public menu: MenuController,
    public app: App,
    public storage: Storage,
    public http: Http,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private geolocation: Geolocation,
    private network: Network,
    private toastCtrl: ToastController,
    private keyboard: Keyboard,

  ) {
    platform.ready().then(async () => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.network.onConnect().subscribe(() => {
        postal.publish({
          channel: 'App',
          topic: 'Network_Resume',
          data: {}
        })
      });
      this.network.onDisconnect().subscribe(() => {
        postal.publish({
          channel: 'App',
          topic: 'Network_Error',
          data: {}
        })
        let toast = this.toastCtrl.create({
          message: "It seems the network has connectivity issue",
          duration: 3000
        });
        toast.present();
      });

      let token = await util.getAccessToken(http, storage);
      if(token !== null)
        this.rootPage = TabsNavigationPage
      else
        this.rootPage = WalkthroughPage
      this.splashScreen.hide();
      this.statusBar.styleDefault();

      let watch = this.geolocation.watchPosition();
      watch.subscribe(async (location) => {
        if(location && location.coords){
          let selfLocation = {lat: location.coords.latitude, lng: location.coords.longitude};
          await storage.set("Self_Location", selfLocation);
        }
      });

      let curLocation = await this.geolocation.getCurrentPosition();
      if(curLocation)
        await storage.set("Self_Location", {lat: curLocation.coords.latitude, lng: curLocation.coords.longitude});


      firebase.auth().onAuthStateChanged(function(user) {
        if(user){
          if(user.emailVerified){
            console.log("User already verified");
          }else
            user.sendEmailVerification(); 
        }else{

        }
      });

      // firebase.auth().signOut().then(function() {
      //   console.log('Signed Out');
      // }, function(error) {
      //   console.error('Sign Out Error', error);
      // });
      let headers = await util.getHttpHeader(this.http, this.storage);
      this.http.post(`${API_URL}/client/auth/pass`, {}, headers).subscribe(
        pass => {
          firebase.auth().signInWithEmailAndPassword(pass.json().email, pass.json().password);
        }
      );

    });

    this.pages = [
      { title: 'Home', icon: 'home', component: TabsNavigationPage },
    ];

    this.pushPages = [
      { title: 'Settings', icon: 'settings', component: SettingsPage }
    ];
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.app.getRootNav().push(page.component);
  }

}
