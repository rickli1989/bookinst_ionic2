import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { TermsOfServicePage } from '../terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';

import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { LoginPage } from '../login/login';

import { FacebookLoginService } from '../facebook-login/facebook-login.service';
import { GoogleLoginService } from '../google-login/google-login.service';
import { SignUpService } from '../signup/signup.service';
import postal from 'postal';
import * as firebase from 'firebase';
import { util } from '../../lib/util';
@Component({
  selector: 'signup-page',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: FormGroup;
  main_page: { component: any };
  login_page: { component: any };
  loading: any;
  subscribe: any;


  constructor(

    public nav: NavController,
    public modal: ModalController,
    public facebookLoginService: FacebookLoginService,
    public googleLoginService: GoogleLoginService,
    public loadingCtrl: LoadingController,
    public signupService: SignUpService
  ) {
    this.main_page = { component: TabsNavigationPage };
    this.login_page = { component: LoginPage };

    this.signup = new FormGroup({
      email: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirm_password: new FormControl('', Validators.required)
    });
  }
  ionViewDidLoad() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    });
  }


  async doSignup(){
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let signup = await this.signupService.signUp({
      ...this.signup.value,
      ...{
        "provider": 'local'
      }
    })
    if(signup){
      this.nav.setRoot(this.login_page.component, {
        email: this.signup.value.email,
        password: this.signup.value.password
      });
    }
    this.loading.dismiss();
  }

  showTermsModal() {
    let modal = this.modal.create(TermsOfServicePage);
    modal.present();
  }

  showPrivacyModal() {
    let modal = this.modal.create(PrivacyPolicyPage);
    modal.present();
  }

}
