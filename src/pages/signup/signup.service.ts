import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import { API_URL } from '../../config/app.config';
import 'rxjs/add/operator/toPromise';
import { util } from '../../lib/util';
import postal from 'postal';
import * as firebase from 'firebase';
@Injectable()
export class SignUpService {
  constructor(public http: Http, public storage: Storage) {
    storage.ready();
  }

  async signUp(formData: any): Promise<any> {
    // await firebase.auth().signInWithEmailAndPassword(formData.email, tmpPassword);
    let res = await firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password);
    let headers = new Headers({ 
      'Content-Type': 'application/json',
      'Authorization': `Basic ${btoa(`${formData.email}:${formData.password}`)}`
    }); 
    let options = new RequestOptions({ headers: headers });

    let registerRes = await this.http.post(
      `${API_URL}/auth/client/register`,
      {
        "email": formData.email,
        "password": formData.password,
        "confirm_password": formData.confirm_password,
        "firstname": formData.firstname,
        "lastname": formData.lastname,
        "dob": formData.dob,
        "avatar": formData.avatar,
        "provider": formData.provider
      }, 
      options
    ).toPromise();

    return res;
  }

  async signUpFn(formData){
    let headers = new Headers({ 
      'Content-Type': 'application/json',
      'Authorization': `Basic ${btoa(`${formData.email}:${formData.password}`)}`
    }); 
    let options = new RequestOptions({ headers: headers });

    let registerRes = await this.http.post(
      `${API_URL}/auth/client/register`,
      {
        "email": formData.email,
        "password": formData.password,
        "confirm_password": formData.confirm_password,
        "firstname": formData.firstname,
        "lastname": formData.lastname,
        "dob": formData.dob,
        "avatar": formData.avatar,
        "provider": formData.provider
      }, 
      options
    ).toPromise();

    postal.publish({
      channel: 'App',
      topic: 'Stop_Loading',
    })
    
    return registerRes;
  }

  async signUpFb(formData: any): Promise<any> {
    try{
      await firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password);
      let res = await this.signUpFn(formData);
      return res;
    }catch(e){
      let res = await this.signUpFn(formData);
      return res;
    }
  }  

  async signUpGoogle(formData: any): Promise<any> {
    try{
      await firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password);
      let res = await this.signUpFn(formData);
      return res;
    }catch(e){
      let res = await this.signUpFn(formData);
      return res;
    }
  }    

}
