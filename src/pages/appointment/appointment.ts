import { Component } from '@angular/core';
import { NavController, SegmentButton, LoadingController } from 'ionic-angular';
import 'rxjs/Rx';
import { BookingDetailPage } from '../booking-detail/booking-detail';
import { AppointmentModel,DisplayAppointmentModel } from './appointment.model';
import { AppointmentService } from './appointment.service';
import moment from 'moment';
import R from 'ramda';
import postal from 'postal';
@Component({
  selector: 'appointment',
  templateUrl: 'appointment.html'
})
export class AppointmentPage {
  segment: string;
  schedule: DisplayAppointmentModel = new DisplayAppointmentModel();
  loading: any;
  bookingInfo :any;
  subscribe: any;
  constructor(
    public nav: NavController,
    public appointmentService: AppointmentService,
    public loadingCtrl: LoadingController
    ) {
    this.segment = "today";
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidEnter() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    })

    const capitalize = R.compose(
      R.join(''),
      R.juxt([R.compose(R.toUpper, R.head), R.tail])
    );
    const capitalizeOrNull = R.ifElse(R.equals(null), R.identity, capitalize);
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.appointmentService
    .getAppointment()
    .then(data => {
      this.bookingInfo = data;
      this.schedule.today = R.map(
        data =>{
          return {
            _id: data._id,
            date:{
              day: moment(data.startTime).format('ddd'),
              month_name: moment(data.startTime).format('MMM'),
              time: moment(data.startTime).format('h:mm a'),
              full: moment(data.startTime).format('MMMM Do YYYY, h:mm a')
            },
            title: data.appTitle,
            location: data.storeId.formattedAddress,
            status: capitalizeOrNull(data.status)
          }

        }, data.today);

      this.schedule.future = R.map(
        data =>{
          return {
            _id: data._id,
            date:{
              day: moment(data.startTime).format('ddd'),
              month_name: moment(data.startTime).format('MMM'),
              time: moment(data.startTime).format('h:mm a'),
              full: moment(data.startTime).format('MMMM Do YYYY, h:mm a')
            },
            title: data.appTitle,
            location: data.storeId.formattedAddress,
            status: capitalizeOrNull(data.status)
          }

        }, data.future);

      this.schedule.history = R.map(
        data =>{
          return {
            _id: data._id,
            date:{
              day: moment(data.startTime).format('ddd'),
              month_name: moment(data.startTime).format('MMM'),
              time: moment(data.startTime).format('h:mm a'),
              full: moment(data.startTime).format('MMMM Do YYYY, h:mm a')
            },
            title: data.appTitle,
            location: data.storeId.formattedAddress,
            status: capitalizeOrNull(data.status)
          }

        }, data.history);

      this.loading.dismiss();

    });
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }

  goToBooking(booking:any) {
    let bookingItem = R.find(R.propEq('_id',booking._id))(this.bookingInfo[this.segment]);
    this.nav.push(BookingDetailPage, {bookingItem});
  }

}
