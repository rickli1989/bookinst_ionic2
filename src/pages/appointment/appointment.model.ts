/**
 * Created by xiaoY on 19/2/17.
 */
export class BookingModel {
    _id: string ;
    staffId: {
        _id: string ;
        updated_at: string ;
        created_at: string ;
        firstname: string ;
        lastname: string ;
        rate: string ;
        storeId: string ;
        userId: string ;
        skills: string ;
        color: string;
    };
    storeId: any ;
    serviceId: {
        _id: string ;
        __v: string ;
        name: string ;
        price: string ;
        duration: string ;
        storeId: string;
    };
    additionInfo: string ;
    healthFundBy: string ;
    preBooked: string ;
    showUp: string ;
    start_time: string ;
    end_time: string ;
    customerId: {
        _id: string ;
        updated_at: string ;
        created_at: string ;
        firstname: string ;
        lastname: string ;
        email: string ;
        customerNumber: string ;
        storeId: string;
    };
    startTime: string ;
    endTime: string ;
    start: string ;
    end: string ;
    color: string ;
    textColor: string ;
    className: string ;
    title: string;

}

export class AppointmentModel {
    today: Array<BookingModel> = [];
    future: Array<BookingModel> = [];
    history: Array<BookingModel> = [];
}

export class EventDate {
    day: string;
    month: string;
    month_name: string;
    time: string;
    full: string;
}


export class DisplayAppointmentModel {
    today: Array<DisplayItemModel> = [];
    future: Array<DisplayItemModel> = [];
    history: Array<DisplayItemModel> = [];
}

export class DisplayItemModel {

    date:{
        day: string,
        month_name: string,
        time: string,
        full: string
    };
    title:string;
    location: string;

}