import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { API_URL } from '../../config/app.config';
import 'rxjs/add/operator/toPromise';
import { AppointmentModel } from './appointment.model';
import { util } from '../../lib/util';
import { Storage } from '@ionic/storage';
import postal from 'postal';
@Injectable()
export class AppointmentService {
  constructor(public http: Http,public storage: Storage) {
    storage.ready();
  }

  async getAppointment():Promise<any>{
    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.get(`${API_URL}/client/booking?normalize=true`, headers)
    .toPromise()
    .then(response => response.json() as AppointmentModel)
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    // postal.publish({
    //   channel: 'App',
    //   topic: 'Stop_Loading',
    // })
    return Promise.reject(error.message || error);
  }

}
