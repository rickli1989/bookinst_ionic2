import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { util } from '../../lib/util';
import { API_URL } from '../../config/app.config';
import { Storage } from '@ionic/storage';
import { ListingModel } from './listing.model';

@Injectable()
export class ListingService {
  constructor(public http: Http, public storage: Storage) {
    storage.ready();
  }

  async getData(): Promise<ListingModel> {
    let headers = await util.getHttpHeader(this.http, this.storage);

    return this.http.get(`${API_URL}/client/industry`,headers)
      .toPromise()
      .then(response => response.json())
  }

}
