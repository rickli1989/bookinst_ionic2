import { Component } from '@angular/core';
import { NavController, LoadingController,ModalController } from 'ionic-angular';

import { FeedPage } from '../feed/feed';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { ListingModel } from './listing.model';
import { ListingService } from './listing.service';
import { util } from '../../lib/util';
import { MapFilterPage } from '../map-filter/map-filter';
import postal from 'postal';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

@Component({
  selector: 'listing-page',
  templateUrl: 'listing.html',
})
export class ListingPage {
  listing: ListingModel = new ListingModel();
  loading: any;
  categories: any;
  getUrl: any;
  subscribe: any;

  constructor(
    public modalController:ModalController,
    public nav: NavController,
    public listingService: ListingService,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public contacts: Contacts,
  ) {
    this.loading = this.loadingCtrl.create();
    this.getUrl = util.getUrl();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.listingService
      .getData()
      .then(data => {
        this.categories = data;
        this.loading.dismiss();
      });
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    });
  }

  search() {
    
  }

  async goToFeed(category: any) {
    await this.storage.set('filter',{
      industry: category,
      distance: ""
    });
    this.nav.push(FeedPage, { category: category });
  }

}
