export class ListingModel {
  stores: Array<ListingItemModel>;
}

export class ListingItemModel {
  _id: string;
  displayText: string;
  name:string
}
