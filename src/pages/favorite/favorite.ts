import { Component,ViewChild } from '@angular/core';
import { NavController, LoadingController, SegmentButton, App, NavParams, ViewController } from 'ionic-angular';
import 'rxjs/Rx';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import { FavoriteService } from './favorite.service';
import { ContactCardPage } from '../contact-card/contact-card';
import { ProfilePage } from '../profile/profile';
import postal from 'postal';

@Component({
  selector: 'favorite-page',
  templateUrl: 'favorite.html'
})
export class FavoritePage {
  loading: any;
  display: string;
  getUrl: any;
  favoriteStore: Array<any>;
  favoriteStaff: Array<any>;
  search: any;
  showSearchBar: boolean;
  displayText: string;
  subscribe: any;

  @ViewChild('searching') searchbar;

  constructor(
    public nav: NavController,
    public favoriteService: FavoriteService,
    public loadingCtrl: LoadingController,
    public uts: UtilService,
    private view: ViewController,
  ) {
    this.display = 'store';
    this.loading = this.loadingCtrl.create();
    this.getUrl = util.getUrl();
    this.uts = uts;
    this.search = "";
    this.showSearchBar = true;
    this.displayText = '';
    this.favoriteStore = [];
  }

  ionViewDidEnter() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    })

    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.favoriteService
      .getData()
      .then(data => {
        this.favoriteStore = data.favorStore;
        this.favoriteStaff = data.favorStaff;
        this.loading.dismiss();
      });
  }

  goToStore(store:any) {
    this.nav.push(ContactCardPage, {store: store});
  }

  goToProfile(staff: any){
    this.nav.push(ProfilePage, {staff: staff});
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }
  showSearch(){
    this.view.showBackButton(false);
    this.showSearchBar = true;
    setTimeout(() => {
      this.searchbar.setFocus();
    });
  }
  onFocus(){
    // console.log(this.searchbar);
    // clearInterval(this.interval);
  }
  onCancel(){
    this.view.showBackButton(true);
    // this.showSearchBar = false;
  }
  onInput(){
    console.log(this.search);
  }

}
