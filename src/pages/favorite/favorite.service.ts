import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { util } from '../../lib/util';
import { Storage } from '@ionic/storage';
import { API_URL } from '../../config/app.config';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FavoriteService {
  constructor(public http: Http, public storage: Storage) {}

  async getData(): Promise<any> {
    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.get(`${API_URL}/client`, headers)
     .toPromise()
     .then(response => response.json())
     .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
