import { Component } from '@angular/core';
import { NavController, Platform, NavParams, AlertController, LoadingController, ViewController, ToastController } from 'ionic-angular';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import { BookingService } from './booking.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { BookingDetailPage } from '../booking-detail/booking-detail';
import postal from 'postal';
import R from 'ramda';
import moment from 'moment';

@Component({
  selector: 'booking',
  templateUrl: 'booking.html'
})

export class BookingPage {
  loading:any;
  checkUrl:any;
  store:any;
  eventSource:any;
  viewTitle:any;
  isToday:boolean;
  staff:any;
  service:any;
  serviceList: any;
  staffList: any;
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };
  event_form: FormGroup;
  duration: any;
  price: any;
  isShowing: boolean;
  preSelectedService: any;
  preSelectedStaff: any;
  preSelectedDay: string;
  isModify: boolean;
  bookingInfo: any;
  store_id: string;
  bookingOfMonth: any;
  bookingList: any;
  scrollHeight: number;
  selectedDate: any;
  subscribe: any;
  minDate: any;
  maxDate: any;
  client: any;

  constructor(
    private platform: Platform,
    private toastCtrl: ToastController,
    public nav: NavController,
    public bookingService: BookingService,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public uts: UtilService,
    private navController: NavController,
    public alertCtrl: AlertController,
    public view: ViewController) {
    
    this.loading = this.loadingCtrl.create();
    this.checkUrl = util.getUrl();
    this.store_id = navParams.get('store_id');
    this.preSelectedService = navParams.get('preSelectedService');
    this.preSelectedStaff = navParams.get('preSelectedStaff');
    this.preSelectedDay = navParams.get('preSelectedDay');
    this.isModify = navParams.get('isModify');
    this.bookingInfo = navParams.get('bookingInfo');
    this.uts = uts;
    this.minDate = moment().format('YYYY');
    this.maxDate = moment().add(6, 'month').format('YYYY-MM-DD');
    this.bookingOfMonth = [];
    this.event_form = new FormGroup({
      serviceSelected : new FormControl('', Validators.required),
      staffSelected : new FormControl('any', Validators.required),
      from_date: new FormControl('2016-09-18', Validators.required),
      from_time: new FormControl('13:00', Validators.required),
      comment : new FormControl('')
    });
    this.isShowing = false;
    this.serviceList = [
      {
        name: 'service',
        options: []
      }
    ];

    this.staffList = [
      {
        name: 'staff',
        options: []
      }
    ];
   
    if(this.preSelectedDay){
      this.calendar.mode = 'day';
      this.calendar.currentDate = moment(this.preSelectedDay,'MMM DD YYYY').toDate();
    }else{
      this.selectedDate = new Date();
    }

  }

  async ionViewDidLoad(){
    this.loading.present();

    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    })

    let data = await this.bookingService.getStoreDetail(this.store_id);
    this.client = await this.bookingService.getClient();

    this.store = data.storeDetail;
    this.staff = data.storeStaff;
    this.service = data.storeService;

    let serviceOptions =  R.map( data => { return {
          text:data.name,
          value:data._id
        }}
        , this.service);

    this.serviceList = [
      {
        name: 'service',
        options: serviceOptions
      }
    ];

    setTimeout(() => {
      let element = document.getElementsByClassName('monthview-container')[0];
      if(element){
        if(this.platform.is('ios')){
          this.scrollHeight= this.platform.height() - element['offsetHeight'] - 160;
        }else{
          this.scrollHeight= this.platform.height() - element['offsetHeight'] - 160;
        }
      }
    }, 0);

    setTimeout(() => {
      if(this.preSelectedService){
        this.event_form.patchValue({ serviceSelected: this.preSelectedService._id, staffSelected: 'any' });
      }else if(this.preSelectedStaff){
        this.event_form.patchValue({  staffSelected: this.preSelectedStaff._id });
      }else if(this.bookingInfo){
        this.event_form.patchValue({ serviceSelected: this.bookingInfo.serviceId._id,
          staffSelected: util.maybeProp('_id', this.bookingInfo.staffId, 'any'),
          from_date: moment(this.bookingInfo.start_time).format('YYYY-MM-DD'),
          from_time: moment(this.bookingInfo.start_time).format('hh:mm')
        });
        this.calendar.currentDate = moment(this.bookingInfo.start_time).toDate();

      }
    }, 0)

    this.event_form.valueChanges.subscribe(async data => {
      if(data.serviceSelected && this.service){
        let selected = R.find(R.propEq('_id',data.serviceSelected))(this.service);
        if(selected){
          this.price = selected.price ;
          this.duration = selected.duration;
        }
        this.isShowing = true;
      }else{
        this.isShowing = false;
      }

      if(data.staffSelected && this.staff && this.calendar.mode == 'day'){
        let bookings = await this.bookingService.getAvailableTime(this.store_id,moment(this.selectedDate).format('YYYY-MM-DD'));
        this.eventSource = R.map(d => {
          return {
            startTime: moment(d.startTime).toDate(),
            endTime: moment(d.endTime).toDate(),
            allDay: false,
            title: d.title
          }
        })(bookings.availability);
        this.eventSource = this.eventSource.filter(
            e => {
              let s = R.find(R.propEq('_id',data.staffSelected))(this.staff)
              if(s){
                return `${s.firstname} ${s.lastname}`== e.title
              }else if(data.staffSelected == 'any'){
                return true
              }

            }
        )
      }
    })
    this.loading.dismiss();
  }
  
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onEventSelected(event) {
    console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
  }

  changeMode(mode) {
    if(this.preSelectedDay)
      this.view.dismiss();
    else
      this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onTimeSelected(ev) {
    this.selectedDate = ev.selectedTime;
  }

  switchView(mode){
    this.calendar.mode = mode;
  }

  onCurrentDateChanged(event:Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();

    this.event_form.patchValue({from_date: moment(event).format('YYYY-MM-DD'), from_time:moment().format('HH:00')});
  }

  async onRangeChanged(ev) {
    if(this.calendar.mode == 'day'){
      let bookings = await this.bookingService.getAvailableTime(this.store_id,moment(this.selectedDate).format('YYYY-MM-DD'));
      this.eventSource = R.map(d => {
        return {
          startTime: moment(d.startTime).toDate(),
          endTime: moment(d.endTime).toDate(),
          allDay: false,
          title: d.title
        }
      })(bookings.availability);

      let staffOptions = R.map(
          data => {return {
            text:data.staff.firstname+' '+data.staff.lastname,
            value:data.staff._id
          }},bookings.roster
      );

      staffOptions = R.prepend({text:'Any',value:'any'},staffOptions);

      this.staffList = [
        {
          name: 'staff',
          options: staffOptions
        }
      ];

    }else{
      let bookings = await this.bookingService.getMyBooking(moment(ev.startTime).format('YYYY-MM-DD'), moment(ev.endTime).format('YYYY-MM-DD'));
      let newBookings = R.map(d => {
        return {
          startTime: moment(d.startTime).toDate(),
          endTime: moment(d.endTime).toDate(),
          allDay: false,
          title: d.appTitle
        }
      })(bookings.booking);
      this.eventSource = newBookings;
      this.bookingList = bookings.booking;
      this.bookingOfMonth = R.map(
          data =>{
            return {
              _id: data._id,
              date:{
                day: moment(data.startTime).format('D'),
                month_name: moment(data.startTime).format('MMM'),
                time: moment(data.startTime).format('h:mm a'),
                full: moment(data.startTime).format('MMMM Do YYYY, h:mm a')
              },
              title:data.appTitle,
              location: data.storeId.formattedAddress
            }
          },bookings.booking);
    }
  }

  markDisabled = (date:Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  };

  dismiss() {
    this.view.dismiss();
  }

  createEvent(){
    const createBooking = () => {
      this.loading = this.loadingCtrl.create({
                        spinner: 'dots'
                      });
      this.loading.present();
      let start = `${this.event_form.value.from_date} ${this.event_form.value.from_time}`;
      let end = moment(start).add(this.duration, 'minutes').format("YYYY-MM-DD HH:mm")
      let eventId =  this.bookingInfo ? this.bookingInfo._id : null;
      this.bookingService.bookService(eventId,this.store._id,
                                        this.event_form.value.serviceSelected,
                                          this.event_form.value.staffSelected,
                                            start,
                                              end,
                                                this.event_form.value.comment
                                      ).then(
          payload => {
            let toast = this.toastCtrl.create({
              message: 'Booking Successful Completed',
              duration: 3000,
              position: 'bottom',
              cssClass: 'toast'
            });
            toast.present();
            this.loading.dismiss();
            this.view.dismiss();
          }

      ).catch(function(e) {
        console.log(e); // "oh, no!"
      })
    }
    if(R.isEmpty(this.client.mobile) || R.isNil(this.client.mobile)){
      let alert = this.alertCtrl.create({
        title: 'Enter your mobile',
        message: 'In order to make a booking, you need to have a valid mobile number saved in your profile.',
        inputs: [
          {
            name: 'mobile',
            placeholder: 'Mobile'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
            }
          },
          {
            text: 'Save',
            handler: data => {
              (async () => {
                await this.bookingService.saveProfile({
                  mobile: data.mobile
                });
                createBooking();
              })()
            }
          }
        ]
      });
      alert.present();
    }else{
      createBooking();
    }
  }

  ngOnChanges(changes: any) {
    console.log(changes);
  }

  goToBooking(booking:any) {
    let bookingItem = R.find(R.propEq('_id',booking._id))(this.bookingList);
    if(!this.bookingInfo){
      this.nav.push(BookingDetailPage, { bookingItem });
    }
  }
}