import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import { util } from '../../lib/util';
import { API_URL } from '../../config/app.config';
import moment from 'moment';
import postal from 'postal';
@Injectable()
export class BookingService {
  constructor(public http: Http, public storage: Storage) {
    storage.ready();
  }

  async getClient() {
    let headers = await util.getHttpHeader(this.http, this.storage);
    let client = await this.http.get(`${API_URL}/client`, headers).toPromise();
    client = await client.json();
    return client;
  }

  async saveProfile(data){
    let headers = await util.getHttpHeader(this.http, this.storage);
    let client = await this.http.put(`${API_URL}/client`, data, headers).toPromise();
    return client;
  }

  async getStoreDetail(storeId:string): Promise<any> {
    let headers = await util.getHttpHeader(this.http, this.storage);


    let storeDetail$ = this.http.get(`${API_URL}/client/store/${storeId}`,headers)
    .toPromise();
    let storeService$ = this.http.get(`${API_URL}/client/store/${storeId}/service`,headers)
    .toPromise();
    let storeStaff$ = this.http.get(`${API_URL}/client/store/${storeId}/staff`,headers)
    .toPromise();

    let [ storeDetailP, storeServiceP, storeStaffP ] = await Promise.all([storeDetail$, storeService$, storeStaff$]);
    let [ storeDetail, storeService, storeStaff ] = await Promise.all([storeDetailP.json(), storeServiceP.json(), storeStaffP.json()]);

    return {
      storeDetail, storeService, storeStaff
    };
  }

  async bookService(id:any, storeId:string, serviceId:string, staffId:string, start:string, end:string, additionalInfo:string):Promise<any>{
    let payload = {};
    if (staffId == 'any'){
      payload = {
        id,storeId, serviceId,start,end,additionalInfo
      }
    }else{
      payload = {
        id,storeId, serviceId,staffId,start,end,additionalInfo
      }
    }

    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.post(`${API_URL}/client/booking`,payload, headers)
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    // postal.publish({
    //   channel: 'App',
    //   topic: 'Stop_Loading',
    // });
    return Promise.reject(error.message || error);
  }

  async getMyBooking(start, end){
    let headers = await util.getHttpHeader(this.http, this.storage);
    end = moment(end).add(-1, 'day');
    return this.http.get(`${API_URL}/client/booking?start=${start}&end=${end}`, headers)
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);
  }

  async getAvailableTime(storeId, date ,staffId = null){
    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.post(`${API_URL}/client/store/${storeId}/availability?start=${date}`, {
      staffId
    }, headers)
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);
  }

}