/**
 * Created by xiaoY on 6/2/17.
 */
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,SegmentButton } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import 'rxjs/Rx';
import { GoogleMap } from '../../components/google-map/google-map';
import { StoreDetialModel } from './store-profile.model';
import { StoreDetailService } from './store-profile.service';
import { BookingPage } from '../booking/booking';
import postal from 'postal';

@Component({
    selector: 'store-profile',
    templateUrl: 'store-profile.html'
})
export class StoreProfilePage {
    storeDetail: StoreDetialModel = new StoreDetialModel();
    store:any;
    loading: any;
    category : any;
    staff : any;
    service : any;
    subscribe: any;

    constructor(
        public nav: NavController,
        public storeDetailService: StoreDetailService,
        public navParams: NavParams,
        public loadingCtrl: LoadingController
    ) {
        this.store = navParams.get('store');
        this.loading = this.loadingCtrl.create();
    }


    async ionViewDidLoad() {
        this.loading.present();
        let data = await this.storeDetailService.getStoreDetail(this.store._id);
        console.log(data);
        this.storeDetail = data.storeDetail;
        this.staff = data.storeStaff;
        this.service = data.storeService;
        this.loading.dismiss();
        this.subscribe = postal.subscribe({
            channel: 'App',
            topic: 'Stop_Loading',
            callback: (data) => {
                this.loading.dismiss();
            }
        });
    }

    onSegmentSelected(segmentButton: SegmentButton) {
        // console.log('Segment selected', segmentButton.value);
    }
    goToBooking(store:any) {
        this.nav.push(BookingPage, {store: store});
    }
}
