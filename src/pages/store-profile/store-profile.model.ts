/**
 * Created by xiaoY on 6/2/17.
 */
/**
 * Created by xiaoY on 4/2/17.
 */
export class StoreDetialModel {
    _id:string;
    updated_at:string;
    created_at:string;
    name:string;
    description:string;
    formattedAddress:string;
    lat:string;
    lng:string;
    timezone:string;
    eventTime:string;
    industry:{
        _id:string ;
        name:string ;
        displayText:string
    };
    address:{
        streetNumber:string ;
        streetName:string ;
        city:string ;
        suburb:string ;
        state:string ;
        country:string ;
        postcode:string
    };
    logoImg:string;
    backgroundImg:string;
    hasHealthFund:string;
    shopNo:string;
    businessHours:{
        endTime:string ;
        startTime:string;
    };
    photo:Array<SliderModel>
}
export class SliderModel{
    src:string;
    thumbnail:string;
}