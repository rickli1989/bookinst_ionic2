/**
 * Created by xiaoY on 19/3/17.
 */
import { Injectable, NgZone } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import { util } from '../../lib/util';
import { API_URL } from '../../config/app.config';


@Injectable()
export class MapsFilterService {


    constructor(public zone: NgZone,public http: Http, public storage: Storage) {
        storage.ready();

    }


    async getIndustry(): Promise<any> {
        let headers = await util.getHttpHeader(this.http, this.storage);

        return this.http.get(`${API_URL}/client/industry`,headers)
            .toPromise()
            .then(response => response.json())
    }


}

