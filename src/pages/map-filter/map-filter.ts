/**
 * Created by xiaoY on 19/3/17.
 */
 import { Component } from '@angular/core';
 import { NavController, LoadingController, NavParams, ToastController,ViewController, SegmentButton } from 'ionic-angular';
 import R from 'ramda';
 import { Storage } from '@ionic/storage';
 import { MapsFilterService } from './map-filter.service';
 import postal from 'postal';

 @Component({
   selector: 'map-filter',
   templateUrl: 'map-filter.html'
 })

 export class MapFilterPage {
   category: any;
   selfLocation: any;
   industries: any;
   distance: any;
   listOptions: any;
   callback: any;
   env: any;
   industry: any;
   industryId: number;
   originIndustry: any;
   subscribe: any;

   constructor(public view: ViewController,
     public navParams: NavParams,
     public storage: Storage,
     public mapsFilterService:MapsFilterService){
     this.category = navParams.get('category');

     this.industry = this.category;
     this.selfLocation = navParams.get('selfLocation');
     this.industries = [];
     this.originIndustry = null;
     this.callback = this.navParams.get("callback");
     this.env = this.navParams.get('env');

   }

   async ionViewDidEnter(){
     this.subscribe = postal.subscribe({
       channel: 'App',
       topic: 'Stop_Loading',
       callback: (data) => {
       }
     });

     let filter =  await this.storage.get('filter');
     this.industry = filter.industry;
     this.distance = filter.distance.toString();

     this.mapsFilterService.getIndustry().then(
       data =>{
         this.originIndustry = data;
         this.industries = [
         {
           name: 'industries',
           options: R.map(
             data => {
               return {
                 text:data.name,
                 value:data._id
               }
             }
             ,this.originIndustry)
         }
         ];
         this.industryId = this.industry._id;
       }
      );
   }

   async dismiss() {
     if(this.originIndustry){
       let industrySelected = R.find(R.propEq('_id', this.industryId), this.originIndustry);
       await this.storage.set("filter", {
         industry: industrySelected,
         distance: this.distance
       });
     }
     this.view.dismiss();
     this.callback(this.env);
   }

   onSegmentChanged(segmentButton: SegmentButton) {
     console.log('Segment changed to', this.distance);
   }

   onSegmentSelected(segmentButton: SegmentButton) {
     // console.log('Segment selected', segmentButton.value);
   }
 }