export class ContactModel {
	images: Array<string> = [];
	name: string;
	rating: number;
	email: string;
  phone: string;
  website: string;
  address: string;

	constructor() {
    this.images = [
			'./assets/images/maps/place-1.jpg',
			'./assets/images/maps/place-2.jpg',
			'./assets/images/maps/place-3.jpg',
			'./assets/images/maps/place-4.jpg'
		];
		this.name = "Railway Cafe";
		this.rating = 4;
		this.email = "contact@ionicthemes.com";
	  this.phone = "555-555-555";
	  this.website = "https://ionicthemes.com";
	  this.address = "7644 sable st";
  }
}

export class StoreDetialModel {
	_id:string;
	updated_at:string;
	created_at:string;
	name:string;
	description:string;
	formattedAddress:string;
	lat:string;
	lng:string;
	timezone:string;
	eventTime:string;
	industry:{
		_id:string ;
		name:string ;
		displayText:string
	};
	address:{
		streetNumber:string ;
		streetName:string ;
		city:string ;
		suburb:string ;
		state:string ;
		country:string ;
		postcode:string
	};
	logoImg:string;
	backgroundImg:string;
	hasHealthFund:string;
	shopNo:string;
	businessHours:{
		endTime:string ;
		startTime:string;
	};
	photo:Array<SliderModel>
}
export class SliderModel{
	src:string;
	thumbnail:string;
}