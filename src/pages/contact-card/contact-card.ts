import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, SegmentButton, ModalController } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { StoreDetailService } from './contact-card.service';
import { ContactModel,StoreDetialModel } from './contact.model';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import { BookingPage } from '../booking/booking';
import { ProfilePage } from '../profile/profile';
import postal from 'postal';
import smartPhoto from 'smartphoto';

@Component({
  selector: 'contact-card-page',
  templateUrl: 'contact-card.html'
})
export class ContactCardPage {
  @ViewChild('slider') slider;
  contact: ContactModel = new ContactModel();
  storeDetail: StoreDetialModel = new StoreDetialModel();
  store : any;
  loading : any;
  category : any;
  checkUrl: any;
  staff : any;
  service : any;
  segment : string;
  photos: any;
  subscribe: any;

  constructor(
    public nav: NavController,
    public storeDetailService: StoreDetailService,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public uts: UtilService,
    public modal: ModalController,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public callNumber: CallNumber,
    public inAppBrowser: InAppBrowser,
    private photoViewer: PhotoViewer,
    private launchNavigator: LaunchNavigator,
  ) {
    this.store = navParams.get('store');
    this.loading = this.loadingCtrl.create();
    this.segment = 'service';
    this.checkUrl = util.getUrl();
    this.uts = uts;
    this.photos = [];
    this.service = [];
    this.staff = [];
  }

  async ionViewDidLoad() {
    this.loading.present();
    let data = await this.storeDetailService.getStoreDetail(this.store._id);
    this.storeDetail = data.storeDetail;
    this.staff = data.storeStaff;
    this.service = data.storeService;
    this.photos = data.storePhoto;
    this.slider.update();
    setTimeout(() => {
      this.slider.autoplay = 2500;
      this.slider.startAutoplay();
    }, 500); // need to wait at least 300ms for sliders.update
    this.loading.dismiss();

    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    })
    if(this.storeDetail.photo && this.storeDetail.photo.length > 0)
      setTimeout(() => {
        new smartPhoto(".js-img-viwer", {
          resizeStyle: 'fit'
        });
      }, 0)
  }

  call(number: string){
    if(number){
      this.callNumber.callNumber(number, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: "Phone number not provided",
        buttons: ['OK']
      });
      alert.present();
    }
  }

  sendMail(email: string){
    if(email){
      this.socialSharing.canShareViaEmail().then(() => {
        this.socialSharing.shareViaEmail("Hi there, I want to book a ...", "Make a booking", [email]).then(() => {
          console.log('Success!');
        }).catch(() => {
          console.log('Error');
        });
      }).catch(() => {
         console.log('Sharing via email is not possible');
      });
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: "Email not provided",
        buttons: ['OK']
      });
      alert.present();
    }
  }

  

  openInAppBrowser(website: string){
    if(website){
      this.inAppBrowser.create(website, '_blank', "location=yes");
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: "Website not provided",
        buttons: ['OK']
      });
      alert.present();
    }
  }

  goToStaffProfile(staff:any){
    this.nav.push(ProfilePage, { staff: staff });
  }
  onSegmentChanged(segmentButton: SegmentButton) {
     console.log('Segment changed to', this.segment);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }
  goToBooking(selectedService:any) {
    let modal = this.modal.create(BookingPage, { store_id: this.store._id, preSelectedService: selectedService });
    modal.present();
  }

  startNav(store:any){

    let options: LaunchNavigatorOptions = {
    };

    this.launchNavigator.navigate([store.lat,store.lng], options)
        .then(

        );
  }

  handleImgClick(image:any){
    this.photoViewer.show(image);
  }

}
