/**
 * Created by xiaoY on 8/2/17.
 */
import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import { util } from '../../lib/util';
import { API_URL } from '../../config/app.config';

@Injectable()
export class StoreDetailService {
    constructor(public http: Http, public storage: Storage) {
        storage.ready();
    }

    async getStoreDetail(storeId:string): Promise<any> {
        let headers = await util.getHttpHeader(this.http, this.storage);


        let storeDetail$ = this.http.get(`${API_URL}/client/store/${storeId}`,headers)
            .toPromise();
        let storeService$ = this.http.get(`${API_URL}/client/store/${storeId}/service`,headers)
            .toPromise();
        let storeStaff$ = this.http.get(`${API_URL}/client/store/${storeId}/staff`,headers)
            .toPromise();
        let storePhoto$ = this.http.get(`${API_URL}/client/store/${storeId}/photo`,headers).toPromise()

        let [ storeDetailP, storeServiceP, storeStaffP, storePhotoP ] = await Promise.all([storeDetail$, storeService$, storeStaff$, storePhoto$]);
        let [ storeDetail, storeService, storeStaff, storePhoto ] = await Promise.all([storeDetailP.json(), storeServiceP.json(), storeStaffP.json(), storePhotoP.json()]);
        // console.log(storeDetail);
        // console.log(storeService);
        // console.log(storeStaff);
        return {
            storeDetail, storeService, storeStaff, storePhoto
        };
        //.then(response => response.json() as StoreDetialModel)
    }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
