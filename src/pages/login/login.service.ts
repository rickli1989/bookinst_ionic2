import { Injectable } from "@angular/core";
import { AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import { API_URL } from '../../config/app.config';
import 'rxjs/add/operator/toPromise';
import postal from 'postal';
import * as firebase from 'firebase';
@Injectable()
export class LoginService {
  constructor(public http: Http, public storage: Storage, public alertCtrl: AlertController) {
    storage.ready();
  }

  showConfirm(email, user) {
    let confirm = this.alertCtrl.create({
      title: 'Email verification required!',
      message: 'Please check the link we sent to ' + email + ', click that link to verify your email.' ,
      buttons: [
        {
          text: 'Close',
          handler: () => {
          }
        },
        {
          text: 'Resend',
          handler: () => {
            user.sendEmailVerification();
          }
        }
      ]
    });
    confirm.present();
  }

  async doLogin(formData: any) {
    await firebase.auth().signInWithEmailAndPassword(formData.email, formData.password);
    if(firebase.auth().currentUser.emailVerified){
      
      let headers = new Headers({ 
        'Content-Type': 'application/json',
        'Authorization': `Basic ${btoa(`${formData.email}:${formData.password}`)}`
      }); 
      let options = new RequestOptions({ headers: headers });

      let token = await this.http.post(
        `${API_URL}/auth/client/login`,
        {
          remember: true
        }, 
        options
      ).toPromise()
      let serverToken = token.json();

      await this.storage.set("Access_Token", serverToken.accessToken.token);
      await this.storage.set("Refresh_Token", serverToken.refreshToken.token);
      return token.json().accessToken.token;
    }else{
      this.showConfirm(formData.email, firebase.auth().currentUser);
    }
  }

}
