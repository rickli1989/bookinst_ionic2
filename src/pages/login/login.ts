import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, ToastController} from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { SignupPage } from '../signup/signup';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { LoginService } from './login.service';
import { FacebookLoginService } from '../facebook-login/facebook-login.service';
import { GoogleLoginService } from '../google-login/google-login.service';
import { SignUpService } from '../signup/signup.service';
import postal from 'postal';
import { util } from '../../lib/util';
import { FIREBASE_CLIENT_PASSOWRD } from '../../config/app.config';

@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: FormGroup;
  main_page: { component: any };
  loading: any;
  subscribe: any;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public facebookLoginService: FacebookLoginService,
    public googleLoginService: GoogleLoginService,
    public loadingCtrl: LoadingController,
    public loginService: LoginService,
    public signupService: SignUpService,
    public toastCtrl: ToastController,
    public storage: Storage,
  ) {
    this.main_page = { component: TabsNavigationPage };

    this.login = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  async doLogin(){
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let response = await this.loginService.doLogin(this.login.value);
    if(response){
      this.nav.setRoot(this.main_page.component);
    }
    this.loading.dismiss();
  }

  async ionViewWillEnter(){
    if(this.navParams.data.email && this.navParams.data.password){
      let toast = this.toastCtrl.create({
        message: 'User create successfully !',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      this.login.setValue({
        email: this.navParams.data.email,
        password: this.navParams.data.password
      })
    }
  }

  doFacebookLogin() {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    // Here we will check if the user is already logged in because we don't want to ask users to log in each time they open the app

    // this.facebookLoginService.getFacebookUser()
    // .then(function(data) {
    //    // user is previously logged with FB and we have his data we will let him access the app
    //   env.nav.setRoot(env.main_page.component);
    // }, function(error){
      //we don't have the user data so we will ask him to log in
   this.facebookLoginService.doFacebookLogin()
    .then(async (res) => {
      let token = await this.signupService.signUpFb({
        "email": res.email,
        "password": FIREBASE_CLIENT_PASSOWRD,
        "confirm_password": FIREBASE_CLIENT_PASSOWRD,
        "firstname": res.first_name,
        "lastname": res.last_name,
        "dob": res.birthday,
        "avatar": res.picture.data.url,
        "provider": 'facebook'
      });
      let serverToken = token.json();
      await this.storage.set("Access_Token", serverToken.accessToken.token);
      await this.storage.set("Refresh_Token", serverToken.refreshToken.token);
      this.loading.dismiss();
      this.nav.setRoot(this.main_page.component);
    }, function(err){
      console.log("Facebook Login error", err);
    });
    // });
  }

  doGoogleLogin() {
    this.loading = this.loadingCtrl.create();

    // Here we will check if the user is already logged in because we don't want to ask users to log in each time they open the app
    let env = this;

    // this.googleLoginService.trySilentLogin()
    // .then(async (data) => {
    //   console.log(data.displayName);
    //   let firstname = data.displayName ? data.displayName.split(" ")[0] : "";
    //   let lastname = data.displayName ? data.displayName.split(" ")[1] : "";
    //   let token = await this.signupService.signUpGoogle({
    //     "email": data.email,
    //     "password": FIREBASE_CLIENT_PASSOWRD,
    //     "confirm_password": FIREBASE_CLIENT_PASSOWRD,
    //     "firstname": firstname,
    //     "lastname": lastname,
    //     "avatar": data.imageUrl,
    //     "provider": 'google'
    //   });
    //   let serverToken = token.json();
    //   await this.storage.set("Access_Token", serverToken.accessToken.token);
    //   await this.storage.set("Refresh_Token", serverToken.refreshToken.token);
    //    // user is previously logged with Google and we have his data we will let him access the app
    //   env.nav.setRoot(env.main_page.component);
    // }, function(error){
      //we don't have the user data so we will ask him to log in
      this.googleLoginService.doGoogleLogin()
      .then(async (data) => {
        let firstname = data.displayName ? data.displayName.split(" ")[0] : "";
        let lastname = data.displayName ? data.displayName.split(" ")[1] : "";
        let token = await this.signupService.signUpGoogle({
          "email": data.email,
          "password": FIREBASE_CLIENT_PASSOWRD,
          "confirm_password": FIREBASE_CLIENT_PASSOWRD,
          "firstname": firstname,
          "lastname": lastname,
          "avatar": data.imageUrl,
          "provider": 'google'
        });
        let serverToken = token.json();
        await this.storage.set("Access_Token", serverToken.accessToken.token);
        await this.storage.set("Refresh_Token", serverToken.refreshToken.token);
        env.loading.dismiss();
        env.nav.setRoot(env.main_page.component);
    //   }, function(err){
    //     console.log("Google Login error", err);
    //   });
    });
  }


  goToSignup() {
    this.nav.push(SignupPage);
  }

  goToForgotPassword() {
    this.nav.push(ForgotPasswordPage);
  }

  ionViewDidLoad() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        if(this.loading)
          this.loading.dismiss();
      }
    });
  }

}
