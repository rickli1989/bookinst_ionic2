import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController, ActionSheetController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';

import { TermsOfServicePage } from '../terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';

import { WalkthroughPage } from '../walkthrough/walkthrough';
import { GoogleMapsService } from "../maps/maps.service";
import { MapsModel, MapPlace } from '../maps/maps.model';
import 'rxjs/Rx';

import { ProfileModel } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';
import { Storage } from '@ionic/storage';
import moment from 'moment';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import postal from 'postal';

@Component({
  selector: 'settings-page',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  map_model: MapsModel = new MapsModel();
  settingsForm: FormGroup;
  // make WalkthroughPage the root (or first) page
  rootPage: any = WalkthroughPage;
  loading: any;
  currentLocation : string;
  profile: any = { user: {} };
  autoCompleteShowing: boolean;
  stopBit: boolean;
  callback: any;
  env:any;
  checkUrl:any;
  subscribe: any;

  constructor(
    public uts: UtilService,
    public navParams:NavParams,
    public nav: NavController,
    public modal: ModalController,
    public loadingCtrl: LoadingController,
    public profileService: ProfileService,
    public storage: Storage,
    public actionSheetCtrl: ActionSheetController,
    public GoogleMapsService: GoogleMapsService

  ) {
    this.stopBit = false;
    this.checkUrl = util.getUrl();
    this.loading = this.loadingCtrl.create();
    this.settingsForm = new FormGroup({
      firstname: new FormControl(),
      lastname: new FormControl(),
      location: new FormControl(),
      description: new FormControl(),
      dob: new FormControl(),
      email: new FormControl(),
      mobile: new FormControl(),
      notifications: new FormControl()
    });
    let env = this;
    this.autoCompleteShowing = false;
    this.callback = this.navParams.get("callback");
    this.env = this.navParams.get('env');
  }

  goBack(){
    this.nav.pop();
    this.callback(this.env);
  }

  ionViewDidLoad() {
    this.settingsForm.valueChanges.subscribe(
        (data:any) => {

          if(data.location != this.currentLocation){

            if(this.stopBit){
              this.stopBit = false;
            }else{
              this.autoCompleteShowing = true;
            }
            this.currentLocation = data.location;
            this.searchPlacesPredictions(data.location);
          }
        }
    );

    this.loading.present();
    this.profileService
      .getClient()
      .then(data => {
        this.profile.user = data.client;
        this.settingsForm.setValue({
          firstname: data.client.firstname || "",
          lastname: data.client.lastname || "",
          email: data.client.email || "",
          dob: data.client.dob ? moment(data.client.dob).format("YYYY-MM-DD") : "",
          mobile: data.client.mobile || "",
          location: data.client.location || "",
          description: data.client.description || "",
          notifications: true
        });
        this.autoCompleteShowing = false;

        this.loading.dismiss();
      });

    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    });
  }

  saveProfile() {
    this.loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Updating'
    });
    this.loading.present();
    this.profileService.saveProfile(this.settingsForm).then(data => {

      this.loading.dismiss();
    })
  }

  async changePicture() {
    // await this.profileService.changeAvatar();

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose photo',
      buttons: [
        {
          text: 'Take a photo',
          handler: () => {
            this.profileService.takePhoto('camera').then(photo => {
              this.profile.user = photo;
            });
          }
        },{
          text: 'Choose from album',
          handler: () => {
            this.profileService.takePhoto('album').then(photo => {
              this.profile.user = photo;
            })
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  async logout() {
    // navigate to the new page if it is not the current page
    await this.storage.remove("Access_Token");
    await this.storage.remove("Refresh_Token");
    this.nav.setRoot(this.rootPage);
  }

  showTermsModal() {
    let modal = this.modal.create(TermsOfServicePage);
    modal.present();
  }

  showPrivacyModal() {
    let modal = this.modal.create(PrivacyPolicyPage);
    modal.present();
  }

  selectSearchResult(place: google.maps.places.AutocompletePrediction){
    this.stopBit = true;
    this.autoCompleteShowing = false;
    let env = this;

    env.map_model.search_query = place.description;
    this.settingsForm.patchValue({  location: place.description });

    env.map_model.search_places_predictions = [];

    // We need to get the location from this place. Let's geocode this place!

  }


  searchPlacesPredictions(query: string){
    let env = this;
    if(query !== "")
    {
      env.GoogleMapsService.getPlacePredictions(query).subscribe(
          places_predictions => {
            env.map_model.search_places_predictions = places_predictions;
            console.log(env.map_model.search_places_predictions)
          },
          e => {
            console.log('onError: %s', e);
          },
          () => {
            console.log('onCompleted');
          }
      );
    }else{
      env.map_model.search_places_predictions = [];
    }
  }
}
