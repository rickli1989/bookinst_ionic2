/**
 * Created by xiaoY on 19/2/17.
 */
 import { Component } from '@angular/core';
 import 'rxjs/Rx';
 import { NavController, SegmentButton, LoadingController, NavParams, AlertController, ModalController, ToastController } from 'ionic-angular';
 import { InAppBrowser } from '@ionic-native/in-app-browser';
 import { CallNumber } from '@ionic-native/call-number';
 import { SocialSharing } from '@ionic-native/social-sharing';
 import { BookingPage } from '../booking/booking';
 import { AppointmentModel,DisplayAppointmentModel } from './booking-detail.model';
 import { AppointmentModifyService } from './booking-detail.service';
 import { UtilService } from '../../lib/service';
 import { util } from '../../lib/util';
 import moment from 'moment';
 import R from 'ramda';
 import {async} from "rxjs/scheduler/async";
 import postal from 'postal';

 @Component({
   selector: 'booking-detail',
   templateUrl: 'booking-detail.html'
 })
 export class BookingDetailPage {
   segment: string;
   schedule: DisplayAppointmentModel = new DisplayAppointmentModel();
   loading: any;
   booking: any;
   checkUrl: any;
   staff : any;
   service : any;
   storeDetail : any;
   isHistory : boolean;
   subscribe: any;


   constructor(
     private toastCtrl: ToastController ,
     public alertCtrl: AlertController,
     public navParams: NavParams,
     public nav: NavController,
     public appointmentModifyService: AppointmentModifyService,
     public loadingCtrl: LoadingController,
     public uts: UtilService,
     public modal: ModalController,
     public socialSharing: SocialSharing,
     public callNumber: CallNumber,
     public inAppBrowser: InAppBrowser,
     ) {
     this.staff = [];
     this.service = [];
     this.segment = "today";
     this.loading = this.loadingCtrl.create();
     this.booking = navParams.get('bookingItem');
     this.booking.startTime = moment(this.booking.start_time).format('MMMM Do YYYY, h:mm a')
     this.checkUrl = util.getUrl();
     this.uts = uts;

     this.isHistory = moment(this.booking.start_time) < moment() ;
   }

   ionViewDidLoad() {
     this.subscribe = postal.subscribe({
       channel: 'App',
       topic: 'Stop_Loading',
       callback: (data) => {
         this.loading.dismiss();
       }
     })
   }

   onSegmentChanged(segmentButton: SegmentButton) {
     // console.log('Segment changed to', segmentButton.value);
   }

   onSegmentSelected(segmentButton: SegmentButton) {
     // console.log('Segment selected', segmentButton.value);
   }

   call(number: string){
     this.callNumber.callNumber(number, true)
     .then(() => console.log('Launched dialer!'))
     .catch(() => console.log('Error launching dialer'));
   }

   sendMail(email: string){
     this.socialSharing.canShareViaEmail().then(() => {
       this.socialSharing.shareViaEmail("Hello, I'm trying this fantastic app that will save me hours of development.", "This app is the best!", [email]).then(() => {
         console.log('Success!');
       }).catch(() => {
         console.log('Error');
       });
     }).catch(() => {
       console.log('Sharing via email is not possible');
     });
   }

   cancelBooking(booking:any){
     this.appointmentModifyService.deleteAppointment(booking).then(
       ()=>{
         let toast = this.toastCtrl.create({
           message: 'Booking Successful Completed',
           duration: 3000,
           position: 'bottom',
           cssClass: 'toast'
         });
         toast.present();
         this.nav.pop();
       }
     )
   }

   openInAppBrowser(website: string){
     this.inAppBrowser.create(website, '_blank', "location=yes");
   }
   
   showConfirm() {
     let confirm = this.alertCtrl.create({
       title: 'Confirmation',
       message: 'Are you sure to cancel this appointment?',
       buttons: [
       {
         text: 'No',
         handler: () => {
           console.log('No clicked');
         }
       },
       {
         text: 'Yes',
         handler: () => {
           this.cancelBooking(this.booking);
           console.log('Agree clicked');
         }
       }
       ]
     });
     confirm.present();
   }

   goToBooking(selectedService:any) {
     let modal = this.modal.create(BookingPage,{store_id:this.booking.storeId._id,bookingInfo:this.booking});
     modal.onDidDismiss(async ()=> {
       let data = await this.appointmentModifyService.getBookingDetailById(this.booking);
       this.booking=data;
       this.booking.startTime = moment(this.booking.start_time).format('MMMM Do YYYY, h:mm')
     })

     modal.present();

   }
 }
