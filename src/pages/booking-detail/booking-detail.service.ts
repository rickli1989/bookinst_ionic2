/**
 * Created by xiaoY on 19/2/17.
 */
import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { API_URL } from '../../config/app.config';
import 'rxjs/add/operator/toPromise';
import { AppointmentModel } from './booking-detail.model';
import { util } from '../../lib/util';
import { Storage } from '@ionic/storage';

@Injectable()
export class AppointmentModifyService {
    constructor(public http: Http,public storage: Storage) {
        storage.ready();
    }

    getData(): Promise<AppointmentModel> {
        return this.http.get('./assets/example_data/schedule.json')
            .toPromise()
            .then(response => response.json() as AppointmentModel)
            .catch(this.handleError);
    }
    async getStoreDetail(storeId:string): Promise<any> {
        let headers = await util.getHttpHeader(this.http, this.storage);


        let storeDetail$ = this.http.get(`${API_URL}/client/store/${storeId}`,headers)
            .toPromise();
        let storeService$ = this.http.get(`${API_URL}/client/store/${storeId}/service`,headers)
            .toPromise();
        let storeStaff$ = this.http.get(`${API_URL}/client/store/${storeId}/staff`,headers)
            .toPromise();

        let [ storeDetailP, storeServiceP, storeStaffP ] = await Promise.all([storeDetail$, storeService$, storeStaff$]);
        let [ storeDetail, storeService, storeStaff ] = await Promise.all([storeDetailP.json(), storeServiceP.json(), storeStaffP.json()]);
        // console.log(storeDetail);
        // console.log(storeService);
        // console.log(storeStaff);
        return {
            storeDetail, storeService, storeStaff
        };
        //.then(response => response.json() as StoreDetialModel)
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    async deleteAppointment(booking:any):Promise<any>{

        let headers = await util.getHttpHeader(this.http, this.storage);
        return this.http.delete(`${API_URL}/client/store/${booking.storeId._id}/events/${booking._id}`, headers)
            .toPromise()
            .then(response => response.json() as AppointmentModel)
            .catch(this.handleError);
    }
    async  getBookingDetailById(booking:any):Promise<any>{

        let headers = await util.getHttpHeader(this.http, this.storage);
        return this.http.get(`${API_URL}/client/store/${booking.storeId._id}/events/${booking._id}`, headers)
            .toPromise()
            .then(response => response.json() as AppointmentModel)
            .catch(this.handleError);
    }

}