import { Component } from '@angular/core';

import { ListingPage } from '../listing/listing';
import { FavoritePage } from '../favorite/favorite';
import { ProfilePage } from '../profile/profile';
import { AppointmentPage } from '../appointment/appointment';
import postal from 'postal';


@Component({
  selector: 'tabs-navigation',
  templateUrl: 'tabs-navigation.html'
})
export class TabsNavigationPage {
  findPage: any;
  favoritePage: any;
  profilePage: any;
  appointmentPage: any;
  sourcePage: any;
  subscribe: any;

  constructor() {
    this.sourcePage = {
      source: 'myself',
      payload: null
    };
    this.findPage = ListingPage;
    this.favoritePage = FavoritePage;
    this.profilePage = ProfilePage;
    this.appointmentPage = AppointmentPage;
  }

  ionViewDidLoad() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
      }
    });
  }

}
