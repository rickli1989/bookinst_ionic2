import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, NavParams,ModalController, Platform } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Keyboard } from '@ionic-native/keyboard';
import { Observable } from 'rxjs/Observable';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import { GoogleMap } from "../../components/google-map/google-map";
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMapsService } from "./maps.service";
import { MapsModel, MapPlace } from './maps.model';
import { ContactCardPage } from '../contact-card/contact-card';
import { MapFilterPage } from '../map-filter/map-filter';
import R from 'ramda';
import { Storage } from '@ionic/storage';
import postal from 'postal';

@Component({
  selector: 'maps-page',
  templateUrl: 'maps.html'
})
export class MapsPage implements OnInit {
  @ViewChild(GoogleMap) _GoogleMap: GoogleMap;
  map_model: MapsModel = new MapsModel();
  loading:any;
  category:any;
  stores:any;
  getUrl:any;
  currentStore:any;
  info:any;
  markerList:any;
  selfLocation:any;
  industries:any;
  subscribe: any;
  storeCards: any;

  constructor(
    public modalController:ModalController,
    public navParams:NavParams,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public GoogleMapsService: GoogleMapsService,
    public uts: UtilService,
    public storage: Storage,
    private geolocation: Geolocation,
    private launchNavigator: LaunchNavigator,
    private keyboard: Keyboard,
    private plt: Platform,
    private nativePageTransitions: NativePageTransitions
  ) {
    this.loading = this.loadingCtrl.create();
    this.uts = uts;
    this.getUrl = util.getUrl();
    this.currentStore = {};
    this.info = new google.maps.InfoWindow();
    this.markerList = [];
    this.selfLocation = {lat:0,lng:0}
    this.industries = [];
    this.storeCards = [];
  }

  ngOnInit() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    });

    this._GoogleMap.$mapReady.subscribe(async map => {
      this.map_model.init(map);
      let filter = await this.storage.get('filter');
      this.category = filter.industry || this.navParams.get('category');

      await this.loadMarkers();
    });
  }

  ionViewDidLoad() {
    let options: NativeTransitionOptions = {
      "direction"      : "right", // 'left|right|up|down', default 'right' (Android currently only supports left and right)
      "duration"       :  1000 // in milliseconds (ms), default 400
      // "iosdelay"       :   50, // ms to wait for the iOS webview to update before animation kicks in, default 60
      // "androiddelay"   :  100,  // same as above but for Android, default 70
      // "winphonedelay"  :  150 // same as above but for Windows Phone, default 200
    };
    if(this.plt.is('core') || this.plt.is('mobileweb')) {
    }else{
      this.nativePageTransitions.flip(options)
          .then( (msg) => console.log(msg) )
          .catch( (err) => console.log(err));
    }
  }


  ionViewWillLeave() {
    // let options: NativeTransitionOptions = {
    //   "direction"      : "left", // 'left|right|up|down', default 'right' (Android currently only supports left and right)
    //   "duration"       :  1000
    // };

    // this.nativePageTransitions.flip(options)
    //     .then( (msg) => console.log(msg) )
    //     .catch( (err) => console.log(err));
  }

  async ionViewDidEnter() {
    let filter = await this.storage.get('filter');
    this.category = filter.industry || this.navParams.get('category');

    this.GoogleMapsService.getIndustry().then(
        data =>{
          this.industries = data;
        }
    );
  }
  
  async loadMarkers() {
    let env = this;
    let filterInfo = await env.storage.get('filter');
    await env.geolocateMe();
    this.GoogleMapsService
      .getStores(env.category._id, filterInfo.distance, env.selfLocation)
        .then(async payload => {
          env.stores = payload;
          env.storeCards = payload.slice(0,10);
          // Create a location bound to center the map based on the results

          env.stores.map(
            (store) => {

              let  icon = {
                url: './assets/images/map-shop-marker.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(23, 32)
              };
              let marker = new google.maps.Marker({
                position: new google.maps.LatLng(store.lat, store.lng),
                title: store.name,
                map: env.map_model.map,
                icon: icon
              });

              marker.addListener('click', () => env.handleCardClick(store) );
              this.markerList.push({ id:store._id, marker:marker });
              }
          )
        });
  }

  loadMoreCards(){

  }

  searchPlacesPredictions(query: string){
    let env = this;

    if(query !== "")
    {
      env.GoogleMapsService.getPlacePredictions(query).subscribe(
        places_predictions => {
          env.map_model.search_places_predictions = places_predictions;
        },
        e => {
          console.log('onError: %s', e);
        },
        () => {
          console.log('onCompleted');
        }
      );
    }else{
      env.map_model.search_places_predictions = [];
    }
  }

  setOrigin(location: google.maps.LatLng){
    let env = this;

    // Clean map
    env.map_model.cleanMap();

    // Set the origin for later directions
    env.map_model.directions_origin.location = location;

    env.map_model.addPlaceToMap(location, '#00e9d5');

    env.map_model.map.panTo(location);
  }

  selectSearchResult(place: google.maps.places.AutocompletePrediction){
    let env = this;

    env.map_model.search_query = place.description;
    env.map_model.search_places_predictions = [];

    // We need to get the location from this place. Let's geocode this place!
    env.GoogleMapsService.geocodePlace(place.place_id).subscribe(
      place_location => {
        env.selfLocation = {
          lat: place_location.lat(),
          lng: place_location.lng(),
        }
        env.updateStoreList(env);
        env.setOrigin(place_location);
      },
      e => {
        console.log('onError: %s', e);
      },
      () => {
        console.log('onCompleted');
      }
    );
  }

  clearSearch(){
    let env = this;
    this.keyboard.close();
    // Clean map
    env.map_model.cleanMap();
  }

  async geolocateMe(){
    let env = this;
    this.loading = env.loadingCtrl.create();
    this.loading.present();

    this.selfLocation = await util.getCurrentLocation(this.storage, this.geolocation);
    let current_location = new google.maps.LatLng(this.selfLocation.lat, this.selfLocation.lng);
    env.GoogleMapsService.geocodeLatLng(this.selfLocation).subscribe(
        (place_location: any) => {
          env.map_model.search_query = place_location;
        },
        e => {
          console.log('onError: %s', e);
        },
        () => {
          console.log('onCompleted');
        }
    );
    env.setOrigin(current_location);
    env.map_model.using_geolocation = true;
    this.loading.dismiss();
  }


  goToStore(store:any) {
    this.nav.push(ContactCardPage, {store: store});
  }

  openGoogleMap(store:any){
    let options: LaunchNavigatorOptions = {
      start: [this.selfLocation.lat,this.selfLocation.lng]
    };

    this.launchNavigator.navigate([store.lat,store.lng], options).then();
  }

  handleCardClick(store:any){
    let current_location = new google.maps.LatLng(store.lat, store.lng);

    this.map_model.map.panTo(current_location);

    this.currentStore = store;

    let content = `<div>
        <div class="place-card current-card" >
          <div class="place-image-heading" style="background-image: url(${this.currentStore.logoImg ? this.getUrl(this.currentStore.logoImg): this.uts.getDefaultStoreImg()})">
            <div class="heading-row">
              <div no-padding  class="col-title">
                <h5 class="place-title">${this.currentStore.name}</h5>
              </div>
            </div>
          </div>
        
        <div class="place-details-container">
            <div class="details-row">
              <div no-padding width-50>
                    <span class="opening-hours" [ngClass]="'opened'">
                      OPENED
                    </span>
              </div>
              <div no-padding width-50>
                <div class="place-rating">
                  <ion-item class="rating-item">
                    <rating ngModel="4" max="5" read-only="true"></rating>
                  </ion-item>
                </div>
              </div>
            </div>
            <div class="details-list" no-lines>
              <div class="place-location">
                <div item-left>
                  <div name="pin"></div>
                </div>
                <span class="location-text">${this.currentStore.formattedAddress}</span>
              </div>
            </div>
          </div>
          <div class="action-buttom">
          <button id="${store._id}_map" class="action-button-map">View On Map</button>
          <button id="${store._id}_store" class="action-button-store">Store Detail</button>
          </div>
        </div>
      </div>`;

    this.info.setContent(content);
    let marker = R.find(R.propEq('id',store._id))(this.markerList);
    this.info.open(this.map_model,marker.marker);
    google.maps.event.addDomListener(document.getElementById(store._id+'_map'),'click',()=>this.openGoogleMap(store));
    google.maps.event.addDomListener(document.getElementById(store._id+'_store'),'click',()=>this.goToStore(store));

  }

  openFilter(){
    let modal = this.modalController.create(MapFilterPage, { category: this.category, selfLocation: this.selfLocation, industries:this.industries, callback: this.updateStoreList, env: this });
    modal.present();
  }

  dismiss(){
    this.nav.pop();
  }

  async updateStoreList(env: any) {
    env.loading = env.loadingCtrl.create();
    env.loading.present();
    let filterInfo = await env.storage.get('filter');
    env.stores = [];
    env.category = filterInfo.industry;
    env.GoogleMapsService.getStores(filterInfo.industry._id, filterInfo.distance, env.selfLocation).then(
      payload => {
        env.stores = payload;
        env.storeCards = payload.slice(0,10);
        //clear the current map
        env.markerList.map(
          m =>{
            m.marker.setMap(null);
          }
        )
        env.stores.map(
          (store) => {
            let  icon = {
              url: './assets/images/map-shop-marker.png',
              // This marker is 20 pixels wide by 32 pixels high.
              size: new google.maps.Size(23, 32)
            };
            let marker = new google.maps.Marker({
              position: new google.maps.LatLng(store.lat,store.lng),
              title: store.name,
              map: env.map_model.map,
              icon: icon
            });

            marker.addListener('click', () => env.handleCardClick(store));
            env.markerList.push({id: store._id, marker: marker});
          }
        )
        env.loading.dismiss();
      }
    )
  }
}
