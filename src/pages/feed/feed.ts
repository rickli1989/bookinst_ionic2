import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ViewController, ModalController, Content } from 'ionic-angular';
import R from 'ramda';
import 'rxjs/Rx';
import { StoreListModel } from './feed.model';
import { FeedService } from './feed.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import { ContactCardPage } from '../contact-card/contact-card';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import { MapsPage } from '../maps/maps';
import { BookingPage } from '../booking/booking';
import { Storage } from '@ionic/storage';
import { MapFilterPage } from '../map-filter/map-filter';
import postal from 'postal';

@Component({
  selector: 'feed-page',
  templateUrl: 'feed.html'
})
export class FeedPage {
  feed: StoreListModel = new StoreListModel();
  loading: any;
  category : any;
  stores:any;
  getUrl: any;
  search: any;
  showSearchBar: any;
  interval: any;
  loader: any;
  selfLocation:any;
  industries:any;
  subscribe: any;
  subscribe1: any;
  distance: number;
  index: any;
  @ViewChild('searching') searchbar;
  @ViewChild(Content) content: Content;

  constructor(
    public modalController:ModalController,
    public nav: NavController,
    public feedService: FeedService,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public uts: UtilService,
    private view: ViewController,
    public modal: ModalController,
    public storage: Storage,
    private geolocation: Geolocation,
    private socialSharing: SocialSharing
  ) {
    this.category = navParams.get('category');
    this.loading = this.loadingCtrl.create();
    this.uts = uts;
    this.getUrl = util.getUrl();
    this.search = "";
    this.showSearchBar = false;
    this.stores = [];
    this.index = 0;
  }

  ionViewWillEnter() {
    this.showSearchBar = false;
  }

  async getStoreData() {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    let filterInfo = await this.storage.get('filter');
    this.distance = filterInfo.distance;
    await this.geolocateMe();
    let payload = await this.feedService.getStores(this.category._id, this.distance, this.selfLocation, this.index, this.search);
    let storeList = R.map(
      s => {
        s.isFavor = payload.client.favorStore ? (R.find(d => d._id == s._id)(payload.client.favorStore) ? true : false) : false
        return s;
      }
    )
    (payload.store)

    this.stores = storeList;
    this.loading.dismiss();
  }

  async ionViewDidLoad() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loader = false;
        this.loading.dismiss();
      }
    })
    await this.getStoreData();
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  goToMap(){
    this.nav.push(MapsPage, { category: this.category });
  }

  showSearch(){
    this.view.showBackButton(false);
    this.showSearchBar = true;
    setTimeout(() => {
      this.searchbar.setFocus();
    });
  }

  async onInput(){
    this.index = 0;
    this.stores = [];
    this.loader = true;
    let filterInfo = await this.storage.get('filter');
    this.distance = filterInfo.distance;
    this.feedService.getStores(this.category._id, this.distance, this.selfLocation, this.index, this.search).then(payload => {
      let storeList = R.map(
          s => {
            s.isFavor = payload.client.favorStore ? (R.find(d => d._id == s._id)(payload.client.favorStore) ? true : false) : false
            return s;
          }
      )
      (payload.store)
      this.stores = storeList;
      this.loader = false;
    });
  }

  virtualTrack(index, item) {
    return item._id;
  }

  onFocus(){
    // console.log(this.searchbar);
    // clearInterval(this.interval);
  }

  async onCancel(){
    this.index = 0;
    this.search = "";
    this.view.showBackButton(true);
    this.showSearchBar = false
    this.stores = [];
    let payload = await this.feedService.getStores(this.category._id, this.distance, this.selfLocation, this.index, this.search);
    let storeList = R.map(
        s => {
          s.isFavor = payload.client.favorStore ? (R.find(d => d._id == s._id)(payload.client.favorStore) ? true : false) : false
          return s;
        }
    )
    (payload.store);

    this.stores = R.concat(this.stores,storeList);
  }

  shareStore(store) {
    //this code is to use the social sharing plugin
    // message, subject, file, url
    this.socialSharing.share(store.description, store.title, store.image)
      .then(() => {
        console.log('Success!');
      })
      .catch(() => {
        console.log('Error');
      });
  }
 
  goToStore(store: any){
    this.nav.push(ContactCardPage, {store: store});
  }

  bookStore(store: any){
    let modal = this.modal.create(BookingPage, { store_id: store._id });
    modal.present();
  }

  addToFavorite(store: Object){
    this.loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Updating'
    });
    this.loading.present();
    this.feedService.addFavorStore(store['_id']).then(data => {
      this.stores = R.map(
        (s) => {
          s.isFavor = R.find(d => d == s._id)(data.favorStore) ? true : false
          return s;
        }
      )(this.stores);
      this.loading.dismiss();
    });
  }

  openFilter(){
    let modal = this.modalController.create(MapFilterPage, { category: this.category, selfLocation: this.selfLocation, industries:this.industries, callback: this.updateStoreList, env: this });
    modal.present();
  }

  async doInfinite(infiniteScroll){
    let filterInfo = await this.storage.get('filter');
    let payload = await this.feedService.getStores(filterInfo.industry._id, filterInfo.distance, this.selfLocation, ++this.index, this.search);
    let storeList = R.map(
      s => {
        s.isFavor = payload.client.favorStore ? (R.find(d => d._id == s._id)(payload.client.favorStore) ? true : false) : false
        return s;
      }
    )
    (payload.store)

    this.stores = R.compose(
      R.flatten,
      R.append(storeList)
    )(this.stores);
    infiniteScroll.complete();
  }

  async updateStoreList(env: any, industry: any, distance: any) {
    env.index = 0;
    env.loading = env.loadingCtrl.create({
      spinner: 'dots',
      content: 'Please wait'
    });
    env.loading.present();
    let filterInfo = await env.storage.get('filter');
    env.stores = [];
    env.category = filterInfo.industry;
    env.feedService.getStores(filterInfo.industry._id, filterInfo.distance, env.selfLocation, env.index, env.search).then(
      payload => {
        let storeList = R.map(
            s => {
              s.isFavor = payload.client.favorStore ? (R.find(d => d._id == s._id)(payload.client.favorStore) ? true : false) : false
              return s;
            }
        )
        (payload.store)
        env.stores = storeList;
        env.loading.dismiss();
      }
    )
  }

  async geolocateMe() {
    let env = this,
        _loading = env.loadingCtrl.create();

    _loading.present();

    this.selfLocation = await util.getCurrentLocation(this.storage, this.geolocation);

    _loading.dismiss();

  }
}
