import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import { util } from '../../lib/util';
import { API_URL } from '../../config/app.config';
import { StoreListModel } from './feed.model';

@Injectable()
export class FeedService {
  constructor(public http: Http, public storage: Storage) {
    storage.ready();
  }

  async getStores(categoryID: string, distance:any, selfLocation:any, index:any, searchString): Promise<any> {
    let headers = await util.getHttpHeader(this.http, this.storage);
    let store$ = this.http.post(`${API_URL}/client/store/search`, {
      searchString,
      lat: selfLocation.lat,
      lng: selfLocation.lng,
      industry: categoryID,
      distance,
      index
    }, headers)
    .toPromise();
    let client$ = this.http.get(`${API_URL}/client`, headers).toPromise();

    let [ $store, $client ] = await Promise.all([store$, client$]);

    let [ store, client ] = await Promise.all([$store.json(), $client.json()]);
    
    return {
      store,
      client
    }
  }

  // async searchStore(searchString, category, selfLocation, index){
  //   let headers = await util.getHttpHeader(this.http, this.storage);

  //   let store$ = this.http.post(`${API_URL}/client/store/search`, {
  //     searchString,
  //     lat: selfLocation.lat,
  //     lng: selfLocation.lng,
  //     industry: category,
  //     index
  //   }, headers).toPromise();
    
  //   let client$ = this.http.get(`${API_URL}/client`, headers).toPromise();

  //   let [ $store, $client ] = await Promise.all([store$, client$]);

  //   let [ store, client ] = await Promise.all([$store.json(), $client.json()]);

  //   return {
  //     store,
  //     client
  //   }
   
  // }

  async addFavorStore(storeId: string){
    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.post(`${API_URL}/client/addFavorStore`, { favorStore: storeId }, headers)
        .toPromise()
        .then(response => response.json())
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  async getIndustry(): Promise<any> {
    let headers = await util.getHttpHeader(this.http, this.storage);

    return this.http.get(`${API_URL}/client/industry`,headers)
        .toPromise()
        .then(response => response.json())
  }

  async updateStores(categoryID:string, distance:any, selfLocation:any): Promise<StoreListModel> {
    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.post(`${API_URL}/client/store/search`, {
          lat: selfLocation.lat,
          lng: selfLocation.lng,
          industry: categoryID,
          distance: distance
        }, headers)
        .toPromise()
        .then(response => response.json())
  }
}
