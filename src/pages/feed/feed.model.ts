export class StoreListModel {
	stores: Array<ListingItemModel>;
	auth:Array<AuthItemModel>
}
export class ListingItemModel {
	_id: string;
	updated_at: string ;
	created_at: string ;
	name: string ;
	description: string ;
	formattedAddress: string ;
	lat: string ;
	lng: string ;
	timezone: string ;
	eventTime: string ;
	logoImg: string ;
	backgroundImg: string ;
	address: {
		streetNumber: string ;
		streetName: string ;
		city: string ;
		suburb: string ;
		state: string ;
		country: string ;
		postcode: string ;
	};
	industry:{
		_id: string ;
		name: string ;
		displayText: string ;
	};
	hasHealthFund:{
	businessHours: string ;
		endTime: string ;
		startTime: string ;
	}
}

export class AuthItemModel{
	role: string;
	storeID: string;
	staffId: string;
}