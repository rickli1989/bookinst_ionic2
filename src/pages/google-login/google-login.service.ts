import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage';
import { GoogleUserModel } from './google-user.model';

@Injectable()
export class GoogleLoginService {

  webClientId: string = "311709244865-rl7kae0nasmap2op8jmnjmqcitab65hn.apps.googleusercontent.com";

  constructor(
    public http: Http,
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage
  ) {}

  trySilentLogin()
  {
    //checks if user is already signed in to the app and sign them in silently if they are.
    let env = this;
    return new Promise<any>((resolve, reject) => {
      this.googlePlus.trySilentLogin({
        'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': this.webClientId, // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        'offline': true
      })
      .then(function (user) {
        resolve(user);
      }, function (error) {
        reject(error);
      });
    });
  }

  doGoogleLogin()
  {
    let env = this;

    return new Promise<any>((resolve, reject) => {

      this.googlePlus.login({
        'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': this.webClientId, // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        'offline': true
      })
      .then(function (user) {
        resolve(user);
      }, function (error) {
        reject(error);
      });
    });
  }

  doGoogleLogout()
  {
    return new Promise((resolve, reject) => {
      this.googlePlus.logout()
      .then(function(response) {
        //user logged out so we will remove him from the NativeStorage
        this.nativeStorage.remove('google_user');
        resolve();
      }, function(error){
        reject(error);
      });
    });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


}
