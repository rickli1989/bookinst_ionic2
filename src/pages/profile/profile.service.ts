import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { util } from '../../lib/util';
import { API_URL } from '../../config/app.config';
import 'rxjs/add/operator/toPromise';
import { ProfileModel } from './profile.model';
import R from 'ramda';
import { AlertController } from 'ionic-angular';
import { Transfer } from '@ionic-native/transfer';
import { Camera } from '@ionic-native/camera';
import * as firebase from 'firebase';
import { AngularFire } from 'angularfire2';
import { UtilService } from '../../lib/service';
import { FirebaseProvider } from '../../providers/firebase';
@Injectable()
export class ProfileService {
  constructor(
    public http: Http, 
    public storage: Storage,
    private camera: Camera,
    private transfer: Transfer,
    private angularfire: AngularFire,
    public uts: UtilService,
    private firebaseProvider: FirebaseProvider,
    public alertCtrl: AlertController,
  ) {}

  getData(): Promise<ProfileModel> {
    return this.http.get('./assets/example_data/profile.json')
     .toPromise()
     .then(response => response.json() as ProfileModel)
     .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  async addFavorStaff(staffId: string): Promise<any> {
    let headers = await util.getHttpHeader(this.http, this.storage);
    return this.http.post(`${API_URL}/client/addFavorStaff`, { favorStaff: staffId }, headers)
        .toPromise()
        .then(response => response.json())
  }

  async getClient(){
    let headers = await util.getHttpHeader(this.http, this.storage);
    let client$ = this.http.get(`${API_URL}/client`, headers).toPromise();
    let summary$ = this.http.get(`${API_URL}/client/summary`, headers).toPromise();
    let [clientP, summaryP] = await Promise.all([client$, summary$]);
    let [client, summary] = await Promise.all([clientP.json(), summaryP.json()]);
    return {
      client,
      summary
    };
    // return R.find(d => d._id == navParams.data.staff._id)(client.favorStaff);
  }

  async getStaff(navParams){

    let headers = await util.getHttpHeader(this.http, this.storage);
    let client$ = await this.http.get(`${API_URL}/client`, headers).toPromise();
    let staff$ = await this.http.get(`${API_URL}/client/store/${navParams.data.staff.storeId._id}/staff/${navParams.data.staff._id}`,headers).toPromise();
    let availability$ = await this.http.get(`${API_URL}/client/store/${navParams.data.staff.storeId._id}/staff/${navParams.data.staff._id}/availability`,headers).toPromise();

    let [ cliendP, staffP , availabilityP ] = await Promise.all([client$, staff$, availability$])
    let [ client, staff, availability ] = await Promise.all([cliendP.json(), staffP.json(), availabilityP.json()]);

    return {
      isFavor:R.find(d => d._id == navParams.data.staff._id)(client.favorStaff),
      staff,
      availability
    }
  }

  async saveProfile(form){
    let message = "", formCorrect = true;
    var mobileRe = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if(mobileRe.test(form.value.mobile)){

    }else{
      formCorrect = false;
      message = "Mobile invalid";
    }

    if(formCorrect){
      this.firebaseProvider.updateProfile({
        name: `${form.value.firstname} ${form.value.lastname}`
      });
      let headers = await util.getHttpHeader(this.http, this.storage);
      let client$ = await this.http.put(`${API_URL}/client`, form.value, headers).toPromise();
      let client = await client$.json();
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: `${message}`,
        buttons: ['OK']
      });
      alert.present();
    }

  }

  async takePhoto(type) {
    let imageData = await this.camera.getPicture({
      sourceType: type == 'album' ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
      allowEdit: true
    });
    let base64Image = imageData;
    let photo = await this.uploadFile(base64Image);
    return photo;
  }


  async uploadFile(image) {
    const fileTransfer = this.transfer.create();
    let token = await util.getAccessToken(this.http, this.storage);
    var options: any;
    options = {
      fileKey: 'file',
      fileName: 'avatar.jpg',
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }
    let upload = await fileTransfer.upload(image, `${API_URL}/client/avatar`, options);
    let headers = await util.getHttpHeader(this.http, this.storage);
    let client = await this.http.get(`${API_URL}/client`, headers).toPromise();
    let res = await client.json();
    this.firebaseProvider.updateProfile({
      img: `${res.avatar}`
    });
    return res;
  }

}
