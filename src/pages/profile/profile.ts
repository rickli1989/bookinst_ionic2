import { Component, ViewChild } from '@angular/core';
import { MenuController, SegmentButton, App, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { ProfileModel } from './profile.model';
import { ProfileService } from './profile.service';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { BookingPage } from '../booking/booking';
import { util } from '../../lib/util';
import { UtilService } from '../../lib/service';
import R from 'ramda';
import { BaseChartDirective } from 'ng2-charts';
import postal from 'postal';

@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  display: string;
  profile: any = { user: {} };
  loading: any;
  showEditBtn: any;
  isFavor: any;
  availability: any;
  staff:any;
  checkUrl: any;
  info: any;
  subscribe: any;

  @ViewChild( BaseChartDirective ) chart: BaseChartDirective;
  barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
    xAxes: [{
                gridLines: {
                    display:false
                }
            }],
    yAxes: [{
                gridLines: {
                    display:false
                }   
            }]
    }
  };
  barChartType:string = 'bar';
  barChartLegend:boolean = false;
  barChartLabels = [ ];
  barChartData = [ { data: [] } ];


  constructor(
    public uts: UtilService,
    public menu: MenuController,
    public app: App,
    public navParams: NavParams,
    public profileService: ProfileService,
    public loadingCtrl: LoadingController,
    public modal: ModalController,
    public photoViewer: PhotoViewer
  ) {
    this.checkUrl = util.getUrl();
    this.display = "grid";
    this.loading = this.loadingCtrl.create();
    this.availability = [];
    this.uts = uts;
  }

  ionViewWillEnter() {
    if(this.navParams.data.source == 'myself'){
      this.showEditBtn = true;
    }else{
      this.showEditBtn = false;
    }
  }

  async ionViewDidEnter() {
    this.subscribe = postal.subscribe({
      channel: 'App',
      topic: 'Stop_Loading',
      callback: (data) => {
        this.loading.dismiss();
      }
    });

    this.loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    this.loading.present();
    if(this.navParams.data.source !== 'myself'){
      let data = await this.profileService.getStaff(this.navParams);
      this.isFavor = data.isFavor;
      this.profile.user = data.staff;
      this.staff = data.staff;
      this.availability = data.availability;
    }else{
      let clientDetail = await this.profileService.getClient();
      let labels = R.map(d => {
        return Object.keys(d)[0];
      })(clientDetail.summary);
      let data = R.map(d => {
        return d[Object.keys(d)[0]]['total'];
      })(clientDetail.summary);
      this.barChartLabels = labels;
      this.chart.labels = labels;
      let clone = JSON.parse(JSON.stringify(this.barChartData));
      clone[0].data = data;
      this.barChartData = clone;
      this.chart.ngOnChanges({});
      this.info = R.map(d => {
        return {
          ...d[Object.keys(d)[0]],
          ...{
            type: Object.keys(d)[0]
          }
        }
      })(clientDetail.summary);
      this.profile.user = clientDetail.client;
    }
    this.loading.dismiss();
  }

  async onPagePop(env:any){
    this.loading = env.loadingCtrl.create({
      spinner: 'dots'
    });
    this.loading.present();

    let clientDetail = await env.profileService.getClient();

    env.profile.user = clientDetail.client;

    this.loading.dismiss();

  }
  goToSettings() {
    // close the menu when clicking a link from the menu
    this.menu.close();
    this.app.getRootNav().push(SettingsPage,{
      callback: this.onPagePop, env: this
    });
  }

  favorStaff() {
    this.loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Updating'
    });
    this.loading.present();
    this.profileService.addFavorStaff(this.navParams.data.staff['_id']).then(data => {
      this.isFavor = !this.isFavor;
      this.loading.dismiss();
    });
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }

  goToBooking(bookingDay: string){
    let modal = this.modal.create(BookingPage, { store_id: this.staff.storeId._id, preSelectedStaff: this.staff, preSelectedDay: bookingDay });
    modal.present();
  }

  chartClicked(e:any):void {
    console.log(e);
  }

  handleImgClick(image:any){
    this.photoViewer.show(image);
  }

  chartHovered(e:any):void {
    console.log(e);
  }

}
