import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { FacebookUserModel } from './facebook-user.model';


@Injectable()
export class FacebookLoginService {
  FB_APP_ID: number = 629275210608913;

  constructor(public http: Http, private nativeStorage: NativeStorage, private facebook: Facebook) {
    this.facebook.browserInit(this.FB_APP_ID, "v2.8");
  }

  doFacebookLogin()
  {
    let env = this;

    return new Promise<any>((resolve, reject) => {
      //["public_profile"] is the array of permissions, you can add more if you need
      this.facebook.login(["public_profile", "email", "user_birthday"]).then((response) => {
        //Getting name and gender properties
        let userId = response.authResponse.userID;
        this.facebook.api(`/${userId}?fields=name,cover,email,birthday,about,context,picture,friends,first_name,last_name`, [])
        .then(function(user) {
          resolve(user);
        })
      }, function(error){
        reject(error);
      });
    });
  }

  doFacebookLogout()
  {
    return new Promise((resolve, reject) => {
      this.facebook.logout()
      .then(function(res) {
        //user logged out so we will remove him from the NativeStorage
        this.nativeStorage.remove('facebook_user');
        resolve();
      }, function(error){
        reject();
      });
    });
  }

  getFacebookUser()
  {
    return this.nativeStorage.getItem('facebook_user');
  }

  setFacebookUser(user: any)
  {
    return new Promise<FacebookUserModel>((resolve, reject) => {
      this.getFriendsFakeData()
      .then(data => {
        resolve(this.nativeStorage.setItem('facebook_user',
          {
            userId: user.id,
            name: user.name,
            gender: user.gender,
            image: "https://graph.this.facebook.com/" + user.id + "/picture?type=large",
            friends: data.friends,
            photos: data.photos
          })
        );
      });
    });
  }

  getFriendsFakeData(): Promise<FacebookUserModel> {
    return this.http.get('./assets/example_data/social_integrations.json')
     .toPromise()
     .then(response => response.json() as FacebookUserModel)
     .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
